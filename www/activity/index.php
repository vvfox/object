<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мероприятия");
?>
<section class="section--main">


<div class="wrapper">
    <h1 class="h1">
    Занимайтесь стрельбой в&nbsp;хорошей компании
    </h1>

    <p class="lid">
    Дни рождения, корпоративы, тимбилдинги и другие праздники до 250 человек — в стрелковом клубе
    </p>
    <div class="btns">
        <a href="#" class="btn btn--red">Оставить завявку</a>
    </div>
</div>


</section>
<section class="section section--events__service">

    <div class="wrapper">
        <div class="lid-tow-column lid">
           
            <p class="">&nbsp;</p>
            <p class="">&nbsp;</p>
        

            <p class="p">За 10 лет работы мы провели более 1000 уникальных мероприятий: от небольших турниров для компании друзей до масштабных праздников с гала-ужином и концертом.</p>
            <p class="p">Умеем находить подход даже с тем, кто боится оружия или относится ко всему очень серьёзно. При этом — сохраняем наш фирменный строжайший подход к безопасности.</p>
        </div>

    </div>
</section>

<section class="section sxtion--events__servielist">
    <div class="wrapper">
        <h2 class="h2">Что мы предлагаем</h2>



        <div class="events--service__list">
            <div class="events--service__item">
                <svg width="64" height="39" viewBox="0 0 64 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.332031 5.86857L1.56963 3.99515L5.68003 5.43199L6.35763 3.27673L52.6136 3.15515L54.9264 0.143311H59.8208V3.15515H63.6176L63.668 9.95805L3.24403 10.5659L0.332031 5.86857ZM3.24963 13.1025H62.688L60.56 16.667L35.1752 16.8991C34.9176 22.9007 32.8848 24.7078 24.7592 24.7078L22.284 24.6691L20.9848 32.9586C23.2248 33.6162 24.0032 36.302 24.0032 38.0759L4.96323 38.3578C4.96323 38.3578 6.79443 30.7646 8.04323 24.6028C9.40963 17.8773 2.56643 15.208 2.56643 15.208L3.24963 13.1025ZM31.6304 16.9709L26.82 17.0152L26.0976 21.3146C30.908 21.2207 31.6192 20.6183 31.6304 16.9323V16.9709ZM36.5864 30.4717C36.5864 27.8136 40.8928 27.1559 40.8928 30.3502C40.8928 33.5444 41.0608 36.6502 41.0608 36.6502L41.7048 38.4738L35.7688 38.3412L36.4184 36.4788C36.4184 36.4788 36.5864 32.4723 36.5864 30.433V30.4717ZM45.1544 30.4717C45.1544 27.8136 49.4608 27.1559 49.4608 30.3502C49.4608 33.5444 49.6288 36.6502 49.6288 36.6502L50.2728 38.4738L44.32 38.3025L44.9696 36.4402C44.9696 36.4402 45.1544 32.4723 45.1544 30.433V30.4717ZM53.672 30.4717C53.672 27.8136 57.984 27.1559 57.984 30.3502C57.984 33.5444 58.152 36.6502 58.152 36.6502L58.7904 38.4738L52.8544 38.3412L53.5096 36.4788C53.5096 36.4788 53.672 32.4723 53.672 30.433V30.4717Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Турнир</h3>
                <p class="p">Выявим лучшего стрелка — и подарим много радости всем участникам</p>
            </div>
            <div class="events--service__item">
                <svg width="71" height="59" viewBox="0 0 71 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M23.1671 8.44106L19.9871 10.3603C18.5329 8.59644 16.1749 6.85381 14.4681 6.86025C14.4658 9.35043 17.9498 13.3817 20.8746 13.5813L21.2135 13.2713L22.7486 13.3334L25.5537 16.2494L22.0268 19.2724L19.4545 16.5906C13.5544 17.1855 6.58115 8.76082 11.765 3.54211C15.6019 -0.319719 21.1648 3.75898 23.1671 8.44106ZM22.7562 27.0719L22.1023 23.6787C22.1023 23.6787 25.5891 19.8719 27.6969 17.6482C30.1827 15.0104 33.2958 15.335 35.3117 20.6448L38.9497 21.6196L39.8907 26.1261C39.8907 26.1261 35.4184 31.1289 33.9029 32.6696C30.9857 35.6442 25.3619 31.6866 25.9547 28.3927L22.7562 27.0719ZM53.2271 39.2275L56.3135 40.0545L57.2319 43.8392C57.2319 43.8392 56.1054 45.0948 54.7344 46.6055L59.937 52.0077L59.3965 58.1614L51.5858 50.062C48.8502 52.8047 45.193 52.0079 43.4955 45.9761L39.8003 45.215L38.9838 40.7533C38.9838 40.7533 43.5971 36.2635 45.3694 34.7401C48.8857 31.7142 53.8337 35.903 53.2271 39.2275ZM32.8843 43.0469L30.7681 39.0443L45.1575 25.2642L48.8229 26.9849L32.8843 43.0469Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Фуршет</h3>
                <p class="p">Организуем для вас легкий ланч, кофе-брейк. Или полноценный банкет</p>
            </div>
            <div class="events--service__item">
                <svg width="68" height="39" viewBox="0 0 68 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M62.4743 21.0918L68 21.1468L67.148 30.2505L62.4311 31.4716C62.1533 27.0205 58.9058 22.7526 53.1084 22.7526C46.9344 22.7526 43.7487 27.454 43.6376 32.131L24.2019 32.2287C24.2637 27.5212 20.9853 22.7526 14.8607 22.7526C8.9152 22.7526 5.77265 27.0755 5.42074 31.5571L0 29.4811C0 29.4811 0.728527 8.50788 0.728527 5.56491C0.728527 2.62194 3.58707 0.332286 7.65571 0.332286C18.3799 0.332286 46.6813 -0.0340593 57.1585 0.00257521C61.647 0.00257521 63.4004 1.22373 64.4129 4.67347C64.4747 4.88718 64.592 5.28405 64.7463 5.7664H56.5843L57.2017 16.482L62.4743 21.0918ZM5.40839 5.95568V15.9813L52.3305 15.6577V5.82135L5.40839 5.95568ZM14.8669 25.03C23.9241 25.03 24.6958 39 14.2495 39C5.36517 39 5.59361 25.03 14.836 25.03H14.8669ZM14.5705 34.9275C18.9355 34.9275 18.6145 29.1087 14.836 29.1087C11.0576 29.1087 10.86 34.9275 14.5397 34.9275H14.5705ZM53.1146 25.03C62.1718 25.03 62.9435 39 52.4972 39C43.6129 39 43.8413 25.03 53.0837 25.03H53.1146ZM52.8182 34.9275C57.1832 34.9275 56.8622 29.1087 53.0837 29.1087C49.3052 29.1087 49.1077 34.9275 52.7874 34.9275H52.8182Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Трансфер</h3>
                <p class="p">Привезем к стрелковому клубу и обратно на комфортном транспорте</p>
            </div>
            <div class="events--service__item">
                <svg width="73" height="37" viewBox="0 0 73 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M46.5273 7.1843L46.4033 0.0730254L65.0142 0.421284C66.1421 0.932443 66.379 5.7407 65.3357 6.66753L46.5273 7.1843ZM15.5091 36.9551L4.29179 37L13.0953 10.7343H43.2676V36.8427L32.885 36.882C30.2625 31.3211 18.0752 31.1076 15.5035 36.9607L15.5091 36.9551ZM21.2729 15.5145L17.5845 25.1872L35.0111 25.3445L38.1524 15.0764L21.2729 15.5145ZM37.5659 5.16214L23.6472 4.60043L21.6282 8.1336L16.4284 8.02125C17.3246 6.65763 18.151 5.24968 18.9042 3.8028C19.9419 1.49416 21.6846 0 24.8823 0C26.2583 0 36.9286 0.0786394 38.6487 0.0786394C41.7618 0.0786394 42.9913 1.38181 43.0082 3.41521C43.0082 4.82511 43.1379 8.5942 43.1379 8.5942L35.7274 8.43692L37.5659 5.16214ZM0 28.1081L1.84981 10.7399H10.095L3.43456 29.5461L0 28.1081ZM59.9329 15.801L63.4971 13.3575L73 13.6552V18.514L69.932 26.1533L70.0448 31.0739L64.2585 31.4503L60.1697 29.1248L49.9506 29.2428L46.8037 27.3554V18.4691L49.6235 16.065L59.9329 15.801Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Фото и видео</h3>
                <p class="p">Профессиональный стрелковый фотограф создаст фото и видеоотчет</p>
            </div>
            <div class="events--service__item">
                <svg width="58" height="54" viewBox="0 0 58 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M58 50.3534L57.1459 53.8551L0.771815 54L0 50.4542L17.6505 44.156L18.2072 39.2876C16.43 36.6545 15.2373 33.6743 14.7088 30.5458C10.4258 25.3625 11.2483 19.1463 13.8231 18.9006C13.8231 17.7733 13.8231 16.6144 13.8231 15.4052C13.8231 7.3247 19.0044 0 29.0063 0C39.0083 0 43.9998 7.08537 43.9998 16.6522V18.9006C46.7897 18.6613 47.8589 25.0791 43.5 30.4451C43.1667 33.5845 42.1032 36.6035 40.3938 39.2624L41.2478 44.3702L58 50.3534ZM37.5532 47.2043C37.4773 46.2911 37.3698 44.5025 37.2749 42.7957C35.9721 43.9959 34.5806 45.097 33.1121 46.0896L25.4826 46.014C24.0598 45.0906 22.7142 44.0543 21.459 42.9153C21.3704 44.8048 21.2692 46.7887 21.2249 47.7523C21.0414 52.0161 37.9391 52.1106 37.5532 47.2043ZM34.4849 33.5941C32.1315 31.5472 26.558 31.5157 24.1287 33.5563C23.0659 38.5003 27.8549 38.3114 28.0321 35.1875H30.6195C30.5689 38.1477 35.4086 38.5948 34.4849 33.5941ZM40.6531 15.1281C40.6531 12.0379 39.4201 9.07428 37.2252 6.8892C35.0303 4.70412 32.0534 3.47656 28.9494 3.47656C25.8454 3.47656 22.8685 4.70412 20.6736 6.8892C18.4787 9.07428 17.2456 12.0379 17.2456 15.1281C17.2456 23.0133 17.7454 27.9384 19.4788 31.6606L25.685 28.2974H32.8274L38.7426 31.6417C40.2862 27.888 40.6531 22.944 40.6531 15.1281Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Персональный менеджер</h3>
                <p class="p">Организацией вашего праздника займется персональный менеджер. </p>
            </div>
            <div class="events--service__item">
                <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.00001 0.0020752C0.881465 0.0020752 -0.0196453 0.919422 0.00032546 2.03779L0.821682 48.0338C0.841155 49.1242 1.73071 49.998 2.82136 49.998H46.9995C48.0885 49.998 48.9774 49.1268 48.9991 48.038L49.9191 2.04207C49.9415 0.922072 49.0398 0.0020752 47.9195 0.0020752H2.00001ZM27.7918 7.50149H12.1412L13.034 43.7486L22.419 42.7486L21.5262 30.6996H27.8953C32.1069 30.6996 35.4209 29.6985 37.8374 27.6963C40.2539 25.6941 41.4621 22.8461 41.4621 19.1523C41.4621 15.286 40.2021 12.369 37.6821 10.4013C35.1275 8.46808 31.8307 7.50149 27.7918 7.50149ZM27.2222 23.8645H21.5262V14.7509H27.015C30.8124 14.7509 32.711 16.2526 32.711 19.2559C32.711 20.7748 32.2277 21.9313 31.2611 22.7253C30.2945 23.4847 28.9482 23.8645 27.2222 23.8645Z" fill="#9E2C33"/>
                </svg>

                <h3 class="h3">Парковка</h3>
                <p class="p">Более 100 парковочных мест на закрытой охраняемой территории</p>
            </div>
        </div>
    </div>
</section>

<section class="section section--events__programms">
    <div class="wrapper">
    <h2 class="h2">Стрелковый праздник для&nbsp;компании</h2>


    <div class="events--programms_overlay">
        <div class="events--programms__text">

        <div class="events--programms__text_over">
            <div class="img--overlay">
                <img src="./../local/templates/theobject/img/events/pogramms.jpg"/> 

            </div>
        

            <p class="p">
                Проведем для участников нескучный инструктаж, расскажем много интересного об оружии и обращении с ним. После разделим всех на две команды и начнем соревнование в меткости и скорости стрельбы. В финале — поздравим команду победителей и выявим лучшего стрелка.
            </p>

        </div>
        
       

            
        </div>
        <div class="events--programms__list">


        <h3 class="h3">Выбирайте программу</h3>
    
    <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"tmp_events_programms", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "18",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "base",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "7",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "tmp_events_programms"
	),
	false
);?>
        </div>
    </div>
        
        


    </div>
</section>

<section class="section section--plan">
    <div class="wrapper">
        <h2 class="h2">Крупнейший стрелковый клуб в&nbsp;России</h2>
        </div>


        <div class="plan--complex" >
            <div class="plan--complex__text">
                <div class="txt">
                    <p class="p">В «Объекте» есть всё для стрельбы на профессиональном уровне, и для организации праздников даже для самой изысканной аудитории.</p>
                    <p class="p">6 больших раздельных стрелковых галерей Медицинский центрБольшая лаунж-зонаМагазин профессионального оборудованияПредставительство Федерации практической стрельбы РФМногофункциональные зоны для организации концерта или кинотеатра</p>
                </div>
            </div>
        </div>
       
        
    
   
</section>
<section class="section section--events__menu-gallery">
    <div class="wrapper">
    <h2 class="h2">
    Ресторан и&nbsp;кондитерская

    </h2>
    <p class="p">
    Ресторан работает под руководством шеф-повара Кирилла Тимофеева. По его руководством мы организуем банкетное меню для вашего праздника — и угостим собственной выпечкой.


    </p>
    <p class="p">
    В нашей команде работают профессиональные повара, официанты, бариста.

    </p>
    </div>

<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "tmp_menu_vars", Array(
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
		"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
		"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
		"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",	// Фильтр товаров
		"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"ELEMENT_SORT_FIELD" => "shows",	// По какому полю сортируем элементы
		"ELEMENT_SORT_FIELD2" => "shows",	// Поле для второй сортировки элементов
		"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
		"ELEMENT_SORT_ORDER2" => "asc",	// Порядок второй сортировки элементов
		"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
		"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
		"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
		"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "content",	// Тип инфоблока
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"LABEL_PROP" => "",	// Свойства меток товара
		"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
		"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
		"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
		"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
		"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",	// Текст кнопки "Показать ещё"
		"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
		"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
		"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
		"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
		"OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Товары",	// Название категорий
		"PAGE_ELEMENT_COUNT" => "18",	// Количество элементов на странице
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
		"PRICE_CODE" => array(	// Тип цены
			0 => "base",
		),
		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
		"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
		"RCM_TYPE" => "personal",	// Тип рекомендации
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_ID" => "6",	// ID раздела
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства раздела
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
		"SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
		"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
		"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
		"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
		"SHOW_OLD_PRICE" => "N",	// Показывать старую цену
		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
		"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
		"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
		"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
		"TEMPLATE_THEME" => "site",	// Цветовая тема
		"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
		"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
		"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
		"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
		"COMPONENT_TEMPLATE" => "tmp_gift_list"
	),
	false
);?>
    
</section>
<section class="section section--events__order">
    <div class="wrapper">
        <div class="section--events__order__layout">

        
        <h2 class="h2">Рассчитайте стоимость  праздника</h2>
        <p class="p">Мы свяжемся с&nbsp;вами в&nbsp;течение 15&nbsp;минут и&nbsp;обсудим детали. </p>
        <?
global $arrFilterSection;
$arrFilterSection = ['SECTION_ID'=>5];

?><?$APPLICATION->IncludeComponent("bitrix:catalog.sections.top", "tmp_events_order", Array(
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
		"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
		"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
		"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
		"ELEMENT_COUNT" => "9",	// Максимальное количество элементов, выводимых в каждом разделе
		"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
		"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
		"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
		"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
		"FILTER_NAME" => "arrFilterSection",	// Имя массива со значениями фильтра для фильтрации элементов
		"HIDE_NOT_AVAILABLE" => "N",	// Не отображать недоступные товары
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "content",	// Тип инфоблока
		"LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы
		"PRICE_CODE" => array(	// Тип цены
			0 => "base",
		),
		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
		"PRODUCT_PROPERTIES" => "",	// Характеристики товара
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"PROPERTY_CODE" => array(	// Свойства
			0 => "css",
			1 => "images",
		),
		"SECTION_COUNT" => "20",	// Максимальное количество выводимых разделов
		"SECTION_FIELDS" => array(	// Поля разделов
			"ID",
"CODE",
"EXTERNAL_ID",
"IBLOCK_ID",
"IBLOCK_SECTION_ID",
"TIMESTAMP_X",
"SORT",
"NAME",
"ACTIVE",
"GLOBAL_ACTIVE",
"PICTURE",
"DESCRIPTION",
"DESCRIPTION_TYPE",
"LEFT_MARGIN",
"RIGHT_MARGIN",
"DEPTH_LEVEL ",
"SEARCHABLE_CONTENT",
"SECTION_PAGE_URL",
"MODIFIED_BY",
"DATE_CREATE",
"CREATED_BY",
"DETAIL_PICTURE"
		),
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
		"SECTION_SORT_FIELD" => "sort",	// По какому полю сортируем разделы
		"SECTION_SORT_ORDER" => "asc",	// Порядок сортировки разделов
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "",
			1 => "",
		),
		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
		"USE_MAIN_ELEMENT_SECTION" => "Y",	// Использовать основной раздел для показа элемента
		"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
		"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?></div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
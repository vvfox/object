<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стрельба");
?>
<section class="section section--main">
    <div class="wrapper">
        <h1 class="h1">Невероятные ощущения — от знакомства с настоящим оружием</h1>
        <p class="lid">Получите навыки стрельбы из&nbsp;боевого оружия: с&nbsp;профессиональными инструкторами и&nbsp;в&nbsp;безопасной обстановке</p>
        <p class="btns"><a href="#courses" class="btn btn--red">Выбрать курс</a></p>
    </div>
</section>
<section class="section section--courses" id="courses">
    <div class="wrapper">
        <h2 class="h2">Начните с базового курса</h2>
        <p class="p subtitle">Расскажем вам об оружии, дадим навыки быстрой и&nbsp;меткой и&nbsp;безопасной стрельбы из&nbsp;него.</p>
        <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"tmp_course_list", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "Y",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Забронировать",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "18",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "base",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "1",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "tmp_course_list",
		"CURRENCY_ID" => "RUB"
	),
	false
);?>
    </div>
</section>
<section class="section">
    <div class="wrapper">

        <div class="gifts--card__attention">
            <img src="/local/templates/theobject/img/gift-cards.png"/>
            <div class="gifts--card__attention--txt">

                <h2 class="h2">Подарите впечатления</h2>
                <p class="p">Любой из наших курсов станет сюрпризом для тех, кто дорог. Закажите подарочную карту в красивом конверте с доставкой по Москве. Или вручите виртуальную карту с персональным дизайном.</p>
                <div class="btns">
                    <a href="/gift/" class="btn btn--border-white">
                        Посмотреть все карты
                    </a>
                </div>
            </div>

          


        </div>
   
    </div>
</section>
<section class="section section--sooting-video">
    <div class="wrapper">

    <h2 class="h2">Стрелять — это интересно. И&nbsp;безопасно</h2>
        

    <div class="shooting--video">

    <div class="video--link">

    <a href="https://www.youtube.com/watch?v=-t3wLkSykI8" class="popup-youtube">
        <div class="video--link__title">
        Отличие стрелковых курсов
        </div>
    </a>

    </div>

    <div class="txt">
    <p class="p">У нас нет монотонных лекций про оружие и скучного занятия, на которой вас будут час учить правильно расставлять ноги. Вместо этого — динамичная тренировка, в процессе которой вы не только узнаете много интересного про оружие от инструкторов, но и получите базовые навыки владения им.</p>
        <p class="p">Наша задача — не сделать из вас спецназовца, а подарить яркие впечатления и новые знания. Мы помогаем понять оружие, учим относиться к нему аккуратно и с уважением. И&nbsp;sконечно, отлично провести время.</p>
   
    </div>

    </div>
</div>
</section>
<section class="section">

    <div class="wrapper">
        <h2 class="h2">У нас всё продумано</h2>

<div class="benefit">
    

        <div class="benefit--list">

            <div class="benefit--item">
                <h3 class="h3">Профессиональные инструкторы</h3>
                <p class="p">В «Объекте» с вами будут заниматься инструкторы, для которых владение оружием и обучение работы с ним — это профессия и дело всей жизни. Инструктор будет всегда рядом, на расстоянии вытянутой руки. Он будет аккуратно контролировать ваши действия, не мешая получать удовольствие от стрельбы.</p>
                <p class="p">Наши инструкторы умеют быть интересными собеседниками и вдохновлять, даже если вы держите оружие первый раз в жизни. На них можно положиться.</p>
        
            </div>
            <div class="benefit--item">
                <h3 class="h3">Безопасность во всём</h3>
                <p class="p">У нас — многоступенчатая система безопасности. Мы попросим вас пройти медицинский контроль перед началом тренировки, после — досмотр (как в аэропорту).</p>
                <p class="p">Мы не делаем никаких исключений из правил безопасности Это важно, потому что вы будете стрелять из настоящего, боевого оружия. Оружие — это опасно, но мы делаем всё для того, чтобы вы получали только радость от его использования.</p>
            </div>
            <div class="benefit--item">
                <h3 class="h3">Индивидуальный подход</h3>
                <p class="p">Мы занимаемся небольшими группами, не более 4 человек в каждой. Так наши инструкторы могут уделять максимум внимания всем участникам тренировки.</p>
                <p class="p">Мы верим, что даже первое занятие — это уникальный шанс получить новые знания. Поэтому инструкторы будут корректировать вашу технику, помогут точнее стрелять и увереннее держать в руках оружие. Вы ощутите прогресс даже спустя 30 минут, и он останется с вами.</p>
            </div>
            <div class="benefit--item">
                <h3 class="h3">Профессиональная экипировка</h3>
                <p class="p">В стрелковой галерее нужно обязательно находиться в защитных очках и наушниках — мы всё выдадим. В «Объекте» используют только профессиональное снаряжение, его аренда входит в стоимость.</p>
            </div>
        
        
        </div>
        <div class="benefit--item__photo">
            <div class="overlay">
                <img src="./../local/templates/theobject/img/photographer.png" />
                <div class="content_txt">
                    <h3 class="h3">Профессиональное фото и видео</h3>
                    <p class="p">Ваше занятие снимет специальный оружейный фотограф. Он знает, как снять вас в самые впечатляющие моменты, а вам не придется отвлекаться на телефон. Снимки и видео отправим в мессенджер после занятия.</p>
                </div>

            </div>
            
     

        </div>

      </div>
    </div>
</section>

<section class="section section--sooting-time">
    <div class="wrapper">
        <h3 class="h3">Ждем вас за 30 минут до начала занятия. За это время вы успеете:</h3>


        <div class="shooting--time">
            <div class="shooting--time_item">
                <div class="duration">5 минут</div>
                <h3 class="h3">Оформить документы</h3>
                <p class="small">Поможем заполнить договор</p> 
            </div>
            <div class="shooting--time_item">
                <div class="duration">15 минут</div>
                <h3 class="h3">Пройти медосмотр у нашего врача</h3>
                <p class="small">Прверим температуру и проведем тест  на алкоголь</p> 
            </div>
            <div class="shooting--time_item">
                <div class="duration">5 минут</div>
                <h3 class="h3">Получить экипировку</h3>
                <p class="small">Выдадим наушники и перчатки. Доплачивать не придется</p> 
            </div>

            <div class="shooting--time_item shooting--time_item__passport">
                <p class="p">Пожалуйста, возьмите с&nbsp;собой паспорт гражданина&nbsp;РФ</p>
                <p class="small">Для участников моложе 14 лет требуется свидетельство о рождении и присутствие одного из родителей.</p>
            </div>





        </div>

        <p class="p">Мы не допускаем до стрельбы нетрезвых людей.</p>





    </div>
</section>

    <section class="section section--club">

    

    <div class="wrapper">


    <div class="club--card">
    <img src="/local/templates/theobject/img/club_card.png" alt="">
    <dv class="club--card__txt">
         <h2 class="h2">Добро пожаловать в&nbsp;клуб</h2>

    <p class="p">В конце занятия мы подарим вам сертификат, который не стыдно поставить на полку (он выглядит необычно, а как — узнаете только лично).</p>
<p class="p"> Также мы выдадим вам именную карту стрелка. С ней вы сможете заниматься тактической стрельбой в «Объекте», сдавать зачеты и совершенствовать навыки. Карта стрелка — это пропуск в мир профессионального стрелкового спорта.
</p>
    <p class="btns">
        <a href="/education/" class="btn btn--red-border">Подробнее о курсе</a>
    </p>

    </div>
   
    </div>

    
    </div>
    </section>


    <?
// включаемая область для раздела
$APPLICATION->IncludeFile('/local/include/shooting_booking.php', Array(), Array(
    "MODE"      => "html",                                           // будет редактировать в веб-редакторе
    "NAME"      => "Бронирование стрельбы",      // текст всплывающей подсказки на иконке
    "TEMPLATE"  => "section_include_template.php"                    // имя шаблона для нового файла
    ));
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
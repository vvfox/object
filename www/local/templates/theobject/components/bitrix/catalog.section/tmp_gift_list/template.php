<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);
?><div class="card--list">
<? foreach ($arResult['ITEMS'] as $arItem) { ?>
	<div class="card--item" data-name="Современное оружие. <?=$arItem['NAME']?>" data-id="<?=$arItem['ID']?>" data-priceid="<?=$arItem['MIN_PRICE']['ID']?>"  data-price="<?=$arItem['MIN_PRICE']['VALUE']?>" >


	<div class="card--img">
		<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
	</div>
	<div class="card--txt">
	<div class="card--title">
		<div class="card--tag">
		<?=$arItem['NAME']?>

		</div>

			
		</div>
		<h3 class="h3">
			<?=$arItem['PREVIEW_TEXT']?>
		</h3>
		<p class="p"><?=$arItem['DETAIL_TEXT']?></p>
		<div class="btns">
			<a href="#" class="btn btn--red">
				<span class="cost"><?=$arItem['MIN_PRICE']['PRINT_VALUE']?></span>
				<span class="txt"><?=$arParams['MESS_BTN_BUY']?></span>
			</a>
		</div>
</div>
	</div>
<?}?>	
</div>
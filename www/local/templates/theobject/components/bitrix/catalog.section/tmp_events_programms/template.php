<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);
?>



<div class="events--programms__list">



<? foreach($arResult['ITEMS'] as $arItem) { ?>
	<div class="events--programms__item">

	

		<p ><b><?=$arItem['NAME']?></b></p>
		<p class="p"><?=$arItem['PREVIEW_TEXT']?></p>
		<p class="p">

		<div class="cost">
		<?=$arItem['MIN_PRICE']['PRINT_VALUE']?> за участника
		</div>
		<div class="duration">


		</div>

		</p>

		
	</div>
	
	<?}?>

	</div>

	<p class="small">
	Гости могут присутствовать на стрельбе бесплатно при соблюдении правил безопасности
	</p>

	<br>
	<br>
	<h3 class="h3">Праздник без стрельбы</h3>

	<p class="p">Организуем для вас&nbsp;корпоратив или&nbsp;фуршет без&nbsp;знакомства с&nbsp;окружением. У&nbsp;нас есть всё&nbsp;для&nbsp;отличного праздника даже для&nbsp;тех, кто&nbsp;не&nbsp;хочет стрелять.</p>
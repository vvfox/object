<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="b-form">
<form id="events--booking__form">
<div class="section--events__order__overlay">





<div class="section--events__order__params">


<div class="">
	

	Гости 
	<div class="input-group">
		<input class="input" type="number" name="count" class="input"  value="10" />
	</div>


</div>
<div class="">
	Дата мероприятия
	<div class="input-group dt">
	&nbsp;

		<input class="input" name="date" type="text" data-type="date" class="input"  />&nbsp;
	</div>
</div>
<?foreach($arResult["SECTIONS"] as $arSection){
	
	if(!in_array($arSection['ID'],[1,2])
	&& 
	
	!in_array($arSection['IBLOCK_SECTION_ID'],[1,2])
	){	?>
		<div class="section--events__order__param-line">
		<? if($arSection["ID"]!=4){?>
			<h3><?=$arSection["NAME"]?></h3>
		<?}?>
		<div class="section--events__order__param section--events__order__param_<?=$arSection["ID"]?>" >
		<?$cell = 0;?>

		<?if($arSection["ID"]==7){?>

			<div class="radio-item" data-id="0" data-cost="0">
				<input type="radio" name="param[7]" id="params-menu-s7-e0" VALUE="0" checked="">
				<label for="params-menu-s7-e0" class="label">
				<div class="title">Без стрельбы</div>
				<div class="cost"><span class="catalog-price">0 ₽</span></div>
				</label>
			</div>

		<?
		$cell = 1;
		}?>
		<?foreach($arSection["ITEMS"] as $arElement){
			if(!in_array($arSection['IBLOCK_SECTION_ID'],[1,2])){
		?>

		
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));
		?>

		<div class="radio-item" data-id="<?=$arElement['ID'];?>" data-cost="<?=$arElement["PRICES"]['base']['VALUE']?>">
		<input type="radio" name="param[<?=$arSection["ID"]?>]" id="params-menu-s<?=$arSection["ID"]?>-e<?=$arElement['ID']?>" VALUE="<?=$arElement["ID"]?>" <?=$cell==0?'checked':''?>>
		<label for="params-menu-s<?=$arSection["ID"]?>-e<?=$arElement['ID']?>" class="label">
				<div class="title">
				<?=$arElement["NAME"]?>

				</div>
				
				<div class="cost">
					<?//print_r($arElement["PRICES"]);?>
					<?$arPrice = $arElement["PRICES"]['base'];?>
						<?if($arPrice["CAN_ACCESS"]){?>
							<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){?>
								<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
							<?}else{?>
								<span class="catalog-price"><?=$arPrice["PRINT_VALUE"]?></span>
							<?}?>
						<?}?>
				</div>
				</label>
				</div>
	<?
	
	 } 
	 $cell++;
	}
		
		?>
		</div>
		</div>
<?}}?>
</div>
<div class="section--events__order__contacts">


<p class="p">Предварительная стоимость проведения мероприятия</p>

<div class="form--group">
	<div class="order--price" id="order--price"></div>
</div>

<div class="form--group">
    <div class="input-group">
        <div class="section--events__order__params-line">
            <input type="text" name="email" id="events__order__email" class="input" data-type="email" >
            <label for="events__order__email" class="label">Электронная почта</label> 
        </div>
    </div>
</div>
<div class="form--group">
    <div class="input-group">
        <div class="section--events__order__params-line">
            <input type="text" name="phone" id="events__order__phone" class="input" data-type="phone" >
            <label for="events__order__phone" class="label">Телефон</label> 
        </div>
    </div>
</div>


<div class="btns">
<button type="submit" class="btn btn--red submit">Оплата картой сейчас</button>
</div>

<p class="small">Отправляя заявку, даёте согласие на обработку персональных данных и принимаете условия пользовательского соглашения. Ознакомьтесь с правилами оплаты и отмены.</p>
</div>
</div>
	</form>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="wrapper">
<?foreach($arResult["SECTIONS"] as $arSection){
	
	
	if(!in_array($arSection['ID'],[1,4])
	
	&& 
	
	!in_array($arSection['IBLOCK_SECTION_ID'],[1,4])
	
	){?>

<div  id="cards-<?=$arSection['ID']?>" >

<?if($arSection['ID']!=2){?>

<h2 class="h2"><?=$arSection["NAME"]?></h2>

<div class="card--direction__description p">
	<div class="card--direction__txt ">
	<p class="p"><?=$arSection["DESCRIPTION"]?></p>
		
	</div>
	
		<a href="/shooting/" class="card--direction__link">
	<svg width="59" height="36" viewBox="0 0 59 36" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" clip-rule="evenodd" d="M0 5.37716L1.15287 3.61764L4.98187 4.96713L5.61308 2.94291L48.7024 2.82872L50.8569 0H55.4162V2.82872H58.9531L59 9.21799L2.71264 9.78893L0 5.37716ZM2.71786 12.1713H58.0871L56.1048 15.519L32.4578 15.737C32.2179 21.3737 30.3242 23.0709 22.7549 23.0709L20.4492 23.0346L19.2389 30.8201C21.3256 31.4377 22.0507 33.9602 22.0507 35.6263L4.31415 35.891C4.31415 35.891 6.01998 28.7595 7.18329 22.9723C8.45615 16.6557 2.08143 14.1488 2.08143 14.1488L2.71786 12.1713ZM29.1557 15.8045L24.6746 15.846L24.0017 19.8841C28.4828 19.7958 29.1453 19.2301 29.1557 15.7682V15.8045ZM33.7724 28.4844C33.7724 25.9879 37.784 25.3702 37.784 28.3702C37.784 31.3702 37.9405 34.2872 37.9405 34.2872L38.5404 36L33.0108 35.8754L33.6159 34.1263C33.6159 34.1263 33.7724 30.3633 33.7724 28.4481V28.4844ZM41.7538 28.4844C41.7538 25.9879 45.7654 25.3702 45.7654 28.3702C45.7654 31.3702 45.9219 34.2872 45.9219 34.2872L46.5218 36L40.9766 35.8391L41.5817 34.09C41.5817 34.09 41.7538 30.3633 41.7538 28.4481V28.4844ZM49.6883 28.4844C49.6883 25.9879 53.7051 25.3702 53.7051 28.3702C53.7051 31.3702 53.8616 34.2872 53.8616 34.2872L54.4563 36L48.9267 35.8754L49.537 34.1263C49.537 34.1263 49.6883 30.3633 49.6883 28.4481V28.4844Z" fill="black"/>
	</svg>

		<p class="p">
		Подробнее о&nbsp;базовых курсах

		</p>
		</a>
	

	

</div>
<?}?>
	<div>
		<?
		$cell = 0;
		foreach($arSection["ITEMS"] as $arElement){
		?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));
		?>

		
		<div class="card-item" 
		data-id="<?=$arElement['ID']?>" 
		data-name="<?=$arElement['NAME']?>" 
		data-cost="<?=$arElement['PRICES']['base']['VALUE_VAT']?>" 
		
		
		style="<?=$arElement['PROPERTIES']['css']['VALUE']?>"id="card-el-<?=$arElement['ID'];?>">

			<div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="card-item__overlay">
			<div class="card-item__col-img">

			<div class="card-images">

					<div class=" carousel-main" id="carousel-main-<?=$arElement['ID']?>">
						<img src="<?=$arElement['PREVIEW_PICTURE']['SRC'];?>"/>
						<? foreach($arElement['PROPERTIES']['images']['VALUE'] as $id_file){?>
							<img src="<?=CFile::GetPath($id_file);?>"/>
						<?}?>


					</div>
					<div  class="carousel carousel-nav" data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false }'>
						<div class="carousel-cell">
						<img src="<?=$arElement['PREVIEW_PICTURE']['SRC'];?>"/>

						</div>
						<? foreach($arElement['PROPERTIES']['images']['VALUE'] as $id_file){?>
							<div class="carousel-cell">
							<img src="<?=CFile::GetPath($id_file);?>"/>
							</div>
						<?}?>

					</div>
			</div>

			<?if($arElement['ID']==7){?>
				<div class="card-item__col-title  <?=($arElement['ID']==7?'visible-xs':'')?>">
					<h2 class="h2"><?=$arElement["NAME"]?></h2>

					
							
					<div class="p">
						<?=$arElement["PREVIEW_TEXT"]?>
					</div>
					<br>
					<br>
				</div>
			<?}?>
					<div class="p">
					<?=htmlspecialcharsBack($arElement['DETAIL_TEXT'])?>
					<br>
					<br>

					</div>
					
					<?if($arElement['ID']==7){?>
						<div class="card-item__col-price">

							<?if($arElement["CAN_BUY"]){?>
								<a href="#" class="btn card--buy <?=$arElement['ID']==7?'btn--red':'btn--dark'?>">

									<span class="cost">
										<?foreach($arElement["PRICES"] as $code=>$arPrice){?>
											<?if($arPrice["CAN_ACCESS"]){?>
												<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){?>
													<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
												<?}else{?>
													<span class="catalog-price"><?=$arPrice["PRINT_VALUE"]?></span>
												<?}?>
											<?}?>
										<?}?>
									</span>
									<span class="txt">
										<?echo GetMessage("CATALOG_BUY")?>
									</span>
								</a>
							<?}?>



						</div>
					<?}?>

				
				</div>
				<div class="card-item__col-txt">
					
					<div class="card-item__col-title  <?=($arElement['ID']==7?'hidden-xs':'')?>">
						<h2 class="h2"><?=$arElement["NAME"]?></h2>
							
						<div class="p">
							<?=$arElement["PREVIEW_TEXT"]?>
						</div>

						<?if($arElement['ID']==7){?>
							<div class="card-item__col-fiz">
								<p class="p">
									Продается в комплекте с фирменной упаковкой. Доставка бесплатная.

								</p>
							
							</div>
						<?}?>
					</div>
					<?if($arElement['ID']!=7 && $arElement['ID']!=6){?>
						<div class="card-item__col-price">

							<?if($arElement["CAN_BUY"]){?>
								<a href="#" class="btn card--buy <?=$arElement['ID']==7?'btn--red':'btn--dark'?>">

									<span class="cost">
										<?foreach($arElement["PRICES"] as $code=>$arPrice){?>
											<?if($arPrice["CAN_ACCESS"]){?>
												<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){?>
													<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
												<?}else{?>
													<span class="catalog-price"><?=$arPrice["PRINT_VALUE"]?></span>
												<?}?>
											<?}?>
										<?}?>
									</span>
									<span class="txt">
										<?echo GetMessage("CATALOG_BUY")?>
									</span>
								</a>
							<?}?>



						</div>
					<?}elseif($arElement['ID']==6 ){?>

						<div class="card--custom__sum">
							<div class="input-group">
								
								<input type="number" id="card-custom-sum" class="input" value="8000" size="6" min="8000"/>
								<label for="card-custom-sum" class="label" >Сумма на карте, ₽</label>
							</div>
							<a href="#" class="btn card--buy <?=$arElement['ID']==7?'btn--red':'btn--dark'?>">	
								<span class="txt">
									<?echo GetMessage("CATALOG_BUY")?>
								</span>
							</a>
						</div>
						
					<?}?>
				</div>
				



			</div>
			
		</div>
	<?}?>
	</div>
	</div>
<?}}?>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


function sortBySORT($a, $b) {
    return $a['SORT'] > $b['SORT'];
}


?>


<div class="wrapper">
    
    <div class="edu--table-of-contents">
      
        
        <div class="edu--toc__over">
            

        <div class="edu--toc__list">
        <h2 class="h2">
            Зачеты
        </h2>
        <ol>
			<?foreach($arResult["ITEMS"] as $arItem){?>
            <li><a href="#less<?=$arItem['ID']?>"><?=$arItem['NAME']?></a></li>
			<?}?>
            <!-- <li><a href="#">Первый выстрел</a></li>
            <li><a href="#">Смена магазина</a></li>
            <li><a href="#">Перенос огня</a></li>
            <li><a href="#">Сильная/слабая рука</a></li>
            <li><a href="#">Стрельба в движении</a></li>
            <li><a href="#">Скорость/безопасность</a></li>
            <li><a href="#">Контроль угла безопасности</a></li>
            <li><a href="#">Бег назад</a></li>
            <li><a href="#">Подвижные мишени</a></li>
            <li><a href="#">Теоретический зачет</a></li> -->
        </ol>

        </div>
        <div class="edu--toc__pdf"> 

        <a href="#">

            <svg width="40" height="46" viewBox="0 0 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.295805 43.8428L0 0L39.6499 0.61524L40 35.0133L28.4576 45.0899L0.295805 43.8428ZM36.1666 3.5695L3.24177 3.20923L3.80924 40.8276L25.1434 41.986L25.5177 32.2696L36.6315 32.0313L36.1666 3.5695ZM7.96257 16.6558L32.0435 16.7722L31.7114 20.3417L8.10142 19.7875L7.96257 16.6558ZM7.62451 9.01798L32.7015 9.62213L32.4177 12.726L7.78147 12.6041L7.62451 9.01798ZM21.9922 26.8045L8.45155 27.4031L8.28856 23.8447L22.6924 23.6452L21.9922 26.8045Z" fill="#9E2C33"/>
            </svg>
            <div class="">PDF-версия<br>документа</div>
        </a>


        </div>
        </div>

        
       


        

    </div>
</div>
</section>

<section class="section section--content">
<?foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <article id="less<?=$arItem['ID']?>" class="less--item">
        <div class="wrapper" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="less--overlay">
				<h2 class="h2"><?echo $arItem["NAME"]?></h2>
				<div class="less--wrapper">
					<div class="less--text">
						
						<?echo $arItem["PREVIEW_TEXT"];?>


						<div class="less--props__list"><?
						
							usort($arItem["DISPLAY_PROPERTIES"], 'sortBySORT');


							foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){?>
								<div class="less--props less--props__<?=$arProperty["CODE"]?>">

									<div class="name">
										<?=htmlspecialcharsBack($arProperty["NAME"])?>
									</div>
									<div class="value">

										<?if($arProperty['CODE']=='scoring_type'){?>

											<span class="tooltype" data-tooltype="<?=$arResult['scoring_type'][ $arProperty["VALUE"]]['PREVIEW_TEXT']?>">
											<?if(is_array($arProperty["DISPLAY_VALUE"])){?>
													<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
												<?}else{?>
													<?=$arProperty["DISPLAY_VALUE"];?>
												<?}?>
											</span>


											
										<?}else{?>
											<?if(is_array($arProperty["DISPLAY_VALUE"])){?>
												<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
											<?}else{?>
												<?=$arProperty["DISPLAY_VALUE"];?>
											<?}?>
										<?}?>
									</div>
								</div>
							<?}?>
						</div>

					</div>
					<div class="less--img">

						<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])){?>
							<img
									class="preview_picture"
									border="0"
									src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
									
									alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
									title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
								
									/>
						<?}?>
						<?if(count($arItem['VIDEO'])>0){?>
							<div class="video--content">
								<h3 class="h3">Видео по теме</h3>
								<div class="video-list"><?
										foreach($arItem['VIDEO'] as $video){?>
										<div class="video--link">
											<a href="<?=$video?>"></a>
										</div>
									<?}?>
								</div>
							</div>
						<?}?>
					</div>
				</div>
			</div>
        </div>
		<div class="less--item__video">
			<div class="wrapper">
				<div class="less--item__video-overlay">
					<div class="less--item__video-txt"></div>
					<div class="less--item__video-iframe" >
						<div class="player" id="player-<?=$arItem['ID']?>"></div>
					</div>
				</div>
			</div>
		</div>
    </article>
	<?}?>
    
</section>




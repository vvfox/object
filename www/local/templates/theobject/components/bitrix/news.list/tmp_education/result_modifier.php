<?


$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_TEXT");
$arFilter = Array("IBLOCK_ID"=>3,  "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);

$arTypes = [];
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arTypes[$arFields['ID']] = $arFields;
}

$arResult['scoring_type'] = $arTypes;


foreach ($arResult['ITEMS'] as $id_itm => $arItem) {
    $videoyt = $arItem['DISPLAY_PROPERTIES']['videoyt']['VALUE'];
    $arResult['ITEMS'][$id_itm]['VIDEO'] = $videoyt;
    unset($arResult['ITEMS'][$id_itm]['DISPLAY_PROPERTIES']['videoyt']);

}
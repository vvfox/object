/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__page_scripts_gift__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__page_scripts_gift___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__page_scripts_gift__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__page_scripts_education__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__page_scripts_education___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__page_scripts_education__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__page_scripts_events__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__page_scripts_events___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__page_scripts_events__);





var ytID = function ytID(url) {
  var video_id, result;

  if (result = url.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/)) {
    video_id = result.pop();
  } else if (result = url.match(/youtu.be\/(.{11})/)) {
    video_id = result.pop();
  }

  return video_id;
};

window.ytID = ytID;

function get_youtube_info(el, quality) {
  var $this = $(el),
      href = $this.attr('href'),
      url = href;
  console.log(el);

  if (url) {
    var video_id, thumbnail, result;
    video_id = ytID(url);

    if (video_id) {
      if (typeof quality == "undefined") {
        quality = 'high';
      }

      var quality_key = 'maxresdefault'; // Max quality

      if (quality == 'low') {
        quality_key = 'sddefault';
      } else if (quality == 'medium') {
        quality_key = 'mqdefault';
      } else if (quality == 'high') {
        quality_key = 'hqdefault';
      }

      var thumbnail = "http://img.youtube.com/vi/" + video_id + "/" + quality_key + ".jpg"; // var url = 'https://www.youtube.com/watch?v=' + video_id;

      var $img = $('<img>'),
          $div_img = $('<div>'),
          $div_title = $('<div>');
      $div_img.addClass('video--thumbnail');
      $div_title.addClass('video--link__title');
      $.getJSON('https://noembed.com/embed', {
        format: 'json',
        url: url
      }, function (data) {
        console.log(data);
        $this.data('title', data.title);
        $div_title.html(data.title);
      });
      $img.attr('src', thumbnail); // console.log(el.href);

      $div_img.append($img);
      $this.prepend($div_title);
      $this.prepend($div_img);
      return thumbnail;
    }
  }

  return false;
}

$(function () {
  $('a[href^="#"]').click(function () {
    var idel = $(this).attr('href'),
        $el = $(idel);

    if (idel != '#' && !$(this).hasClass('tab')) {
      $('html,body').animate({
        scrollTop: $el.offset().top
      }, 'slow');
      return false;
    }
  });
  var tooltp_class = 'tooltype__text';
  $('.tooltype').hover(function () {
    var $this = $(this),
        tooltp_txt = $this.data('tooltype');

    if (!$('.' + tooltp_class).length) {
      $this.append(['<div class="' + tooltp_class + '">', tooltp_txt, '</div>'].join(''));
    }
  }, function () {
    $('.' + tooltp_class).remove();
  });
  var $tabs = $('.tabs'),
      $tab = $('.tab');
  $tab.click(function () {
    var $this = $(this),
        idtab = $this.attr('href');
    $(this).closest('.tabs').find('.tab').removeClass('select');
    $(this).addClass('select');

    if ($(idtab).length) {
      var $tb_container = $(idtab).closest('.tab-container');
      $tb_container.find('.tab-content').removeClass('show');
      $(idtab).addClass('show');
    }

    return false;
  });
  $tabs.each(function (ind, el) {
    var $tab_first = $(el).find('.tab').first();
    $tab_first.click();
  });
  var $price = $('input[name="price"]');
  $('.courses--item').click(function () {
    var $this = $(this),
        name = $this.data('name'),
        price = $this.data('price'),
        product_id = $this.data('id');
    $('.shooting--popup').addClass('show');
    $('.shooting--popup__title').html(name);
    $('input[name="price"]').val(price); //product_id

    $('input[name="product_id"]').val(product_id);
    $('input[name="count_shooters"]:checked').change(); // product_id
    // price

    return false;
  }); // $price 

  $('.shooting--popup__shadows').click(function () {
    $('.shooting--popup').removeClass('show');
  });
  $('input[name="count_shooters"]').change(function () {
    var count_shooters = parseInt($(this).val()),
        price = parseFloat($price.val()),
        sum = (count_shooters * price).toFixed(2);
    $('.sum-value').html(sum);
  });
  var $shooting_popup = $('.shooting--popup');

  if ($shooting_popup.length) {
    $('.shooting--booking__step').first().addClass('active'); // $('.shooting--booking__calendar-input').calendar({});
  }

  (function common(window, $) {
    var $form = $('#shooting--booking__form'),
        $formParent = $form.closest('.b-form');
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);

    function onResponse($form, $formParent, class_name, resp) {
      console.log($form);
      console.log($formParent);
      console.log(class_name);

      if (typeof resp !== 'undefined') {}

      $form.addClass('form--' + class_name);
    }

    $form.formalizer({
      url: '/ajax/booking.php',
      // watchDay: true,
      successCallback: function successCallback(resp) {
        onResponse($form, $formParent, window.classes.success, resp);
      },
      errorCallback: function errorCallback() {
        onResponse($form, $formParent, window.classes.error);
      },
      pluginOpts: {
        range: {
          postfix: '.00',
          min_interval: 1
        },
        phone: {
          mask: '+9(999)-999-99-99'
        },
        date: {
          pickmeup: {
            flat: true,
            hide_on_select: false,
            select_year: false,
            min: minDate
          }
        }
      }
    });
    $('.shooting--booking__calendar-input').change(function () {
      console.log(this.value);
      change_time('day', this.value);
    });
    $('.time-today input').change(function () {
      console.log(this.value);
      change_time('hour', this.value);
    });

    var change_time = function change_time(prop, value) {
      var day_txt = $('.shooting--booking__calendar-input').val(),
          time_txt = $('.time-today input:checked').val(),
          ar_day = day_txt.split('.'),
          day_date = new Date(ar_day[2], ar_day[1], ar_day[0]),
          months = ['', "января", "яевраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
          s_day = [ar_day[0], months[parseInt(ar_day[1])]].join(' '),
          tooltype_text = [s_day, time_txt].join(' ');

      if (day_txt != '' && time_txt != '' && prop == 'hour') {
        $('.tab-contact').click();
      } // console.log(tooltype_text);


      $('.tab-time .tooltip').html(tooltype_text);
    };
  })(window, jQuery);

  var $courses_list = $('.courses--list');

  if ($courses_list.length) {
    var init_flickity = false,
        fl_options = {
      // options
      cellAlign: 'center',
      contain: true,
      // autoPlay: 10000,
      // freeScroll: true,
      wrapAround: true,
      prevNextButtons: false
    };
    $(window).resize(function () {
      if ($(window).width() < 767) {
        if (!init_flickity) {
          $courses_list.flickity(fl_options);
          init_flickity = true;
        }
      } else {
        if (init_flickity) {
          $courses_list.flickity('destroy');
          init_flickity = false;
        }
      }
    }).resize();
  }

  $('.popup-youtube, .popup-vimeo, .popup-gmaps, .popup-yamaps').magnificPopup({
    disableOn: 0,
    type: 'iframe',
    preloader: true,
    fixedContentPos: true
  });
  $('.video--link a').each(function (ind, el) {
    get_youtube_info(el);
  });
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utilities__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utilities___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__utilities__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mediaChecker__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mediaChecker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__mediaChecker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__freezer__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__freezer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__freezer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__calendar__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__formalizer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__formalizer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__formalizer__);






/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function utilities(window, document, $) {
  window.classes = {
    active: 'active',
    popupOpened: 'popup-opened',
    disabled: 'disabled',
    fixed: 'fixed',
    hold: 'hold',
    init: 'init',
    open: 'open',
    success: 'success',
    error: 'error',
    result: 'result',
    freeze: 'freeze',
    hidden: 'hidden',
    shaded: 'shaded',
    shrink: 'shrink',
    ready: 'ready',
    hover: 'hover',
    done: 'done',
    loading: 'loading'
  };
  window.events = {
    ytReady: 'youTubeApiReady',
    play: 'play',
    stop: 'stop',
    pause: 'pause'
  };

  window.roundTo = function (x, n) {
    if (isNaN(x) || isNaN(n)) return false;
    var m = Math.pow(10, n);
    return Math.round(x * m) / m;
  };

  window.ucFirst = function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  window.getDay = function (date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
  };

  window.getNextDay = function (from, daysCount) {
    var nextDay = new Date(from.getTime());
    nextDay.setDate(nextDay.getDate() + daysCount);
    return new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate());
  };

  window.getOffsetRect = function (elem) {
    var box = elem.getBoundingClientRect();
    var body = document.body;
    var docElem = document.documentElement;
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    return {
      top: Math.round(top),
      left: Math.round(left),
      width: box.width,
      height: box.height
    };
  };

  window.getChar = function (event) {
    if (event.which === null) {
      if (event.keyCode < 32) return null;
      return String.fromCharCode(event.keyCode);
    }

    if (event.which !== 0 && event.charCode !== 0) {
      if (event.which < 32) return null;
      return String.fromCharCode(event.which);
    }

    return null;
  };

  window.enterFullScreen = function (element) {
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  };

  window.exitFullScreen = function () {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  };

  window.isFullScreen = function () {
    return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
  };

  window.onGoogleMapsApiReady = function () {
    $(window).trigger('googleMapsApiReady');
  };

  window.loadGoogleMapsApi = function () {
    var $script = $('<script async defer></script>');
    $script.attr('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDZ1TXC6qzkLOU_hPKB4Rkx7yx3EpS3P1k&callback=onGoogleMapsApiReady');
    $('head').append($script);
  };

  window.onYouTubeIframeAPIReady = function () {
    $(window).trigger(window.events.ytReady);
  };

  window.loadYTApi = function () {
    var $script = $('<script async defer></script>');
    $script.attr('src', 'https://www.youtube.com/iframe_api');
    $('head').append($script);
  };
})(window, document, jQuery);

/***/ }),
/* 4 */
/***/ (function(module, exports) {

(function mediaChecker(global, document, $) {
  function MediaChecker(options) {
    this.options = $.extend(true, {}, this.defaults, options);
    $.proxy(init, this)();
  }

  MediaChecker.prototype.defaults = {
    breakPoints: {
      mobileSmall: 320,
      mobile: 767,
      tabletPortrait: 800,
      tablet: 1024
    }
  };

  function init() {
    var self = this;
    $.each(self.options.breakPoints, function (name, value) {
      self.addBreakpoint(name, value);
    });
  }

  MediaChecker.prototype.check = function (brPoint) {
    return global.matchMedia(getQuery(brPoint)).matches;
  };

  MediaChecker.prototype.addBreakpoint = function (name, brPoint) {
    var self = this,
        fnName = 'is' + global.ucFirst(name);

    self[fnName] = function () {
      return self.check(brPoint);
    };
  };

  function getQuery(brPoint) {
    return 'screen and (max-width: ' + brPoint + 'px)';
  }

  global.mediaChecker = new MediaChecker();
})(window, document, jQuery);

/***/ }),
/* 5 */
/***/ (function(module, exports) {

$(function () {
  (function freezer(window, document, $) {
    function Freezer() {
      this.$body = $('html, body');
      this.$content = $('.content');
      this.$window = $(window);
      this.st = this.$window.scrollTop();
      this.freezeCount = 0;
    }

    Freezer.prototype.freeze = function () {
      var self = this,
          st = self.$window.scrollTop();

      if (self.freezeCount === 0) {
        self.$body.addClass(window.classes.freeze);
        self.$content.css({
          'margin-top': -st
        });
        self.st = st;
      }

      self.freezeCount++;
    };

    Freezer.prototype.unFreeze = function (reset) {
      var self = this,
          st = self.st;
      self.freezeCount = Math.max(0, self.freezeCount - 1);

      if (self.freezeCount === 0) {
        self.$body.removeClass(window.classes.freeze);
        self.$content.removeAttr('style');

        if (!reset && st > 0) {
          self.$window.scrollTop(st);
        }

        self.st = 0;
      }
    };

    window.Freezer = new Freezer();
  })(window, document, jQuery);
});

/***/ }),
/* 6 */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function calendar(global, document, $, pickmeup) {
  pickmeup.defaults.locales['ru'] = {
    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
  };

  function Calendar(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Calendar.prototype.defaults = {
    pickmeup: {
      hide_on_select: true,
      class_name: 'calendar',
      locale: 'ru',
      format: 'd.m.Y'
    },
    custom: {
      append: true,
      disablePastDays: false,
      minDay: getMinDay()
    }
  };

  Calendar.prototype.getDate = function (formatted) {
    var self = this,
        element = self.element;
    return pickmeup(element).get_date(formatted);
  };

  Calendar.prototype.setDate = function (date) {
    var self = this,
        element = self.element;
    pickmeup(element).set_date(date);
    return self;
  };

  Calendar.prototype.refresh = function (opts) {
    var self = this;
    self.options = $.extend(true, {}, self.options, opts);
    $.proxy(destroy, self)();
    $.proxy(init, self)();
  };

  function destroy() {
    pickmeup(this.element).destroy();
  }

  function init() {
    var self = this,
        element = self.element,
        $element = self.$element,
        calOptions = self.options.pickmeup,
        minDay = self.options.custom.minDay;
    if (self.options.custom.disablePastDays) calOptions = $.extend({}, self.options.pickmeup, {
      min: minDay,
      render: disablePastDays(minDay)
    });
    pickmeup(element, calOptions);
    if (self.options.custom.append) $element.parent().append($(element.__pickmeup.element));
    $element.on('pickmeup-change', function () {
      $element.trigger('change');
    });
  }

  function disablePastDays(day) {
    return function (date) {
      if (date < day) {
        return {
          disabled: true,
          class_name: 'date-in-past'
        };
      }

      return {};
    };
  }

  function getMinDay() {
    var now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }

  $.fn.calendar = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('calendar');

      if (!data) {
        data = new Calendar(this, _typeof(options) === 'object' && options);
        $this.data('calendar', data);
      }
    });
  };

  $.fn.calendar.Constructor = Calendar;
})(window, document, jQuery, pickmeup);

/***/ }),
/* 7 */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function formalizer(global, document, $, undefined) {
  function Formalizer(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Formalizer.prototype.defaults = {
    structure: {
      children: {
        input: 'input',
        select: 'input.select',
        dayRadio: 'input.day',
        file: 'input.file',
        rangeSlider: 'input.range-slider',
        timeSlider: 'input.range-slider.time-slider',
        agreement: 'input.agreement',
        textarea: 'input.textarea',
        options: 'input-group .options',
        submit: 'submit'
      }
    },
    fn: {
      name: function name($input) {
        $input.on('keypress', function (event) {
          var keyChar = global.getChar(event);
          if (keyChar === null || !keyChar.match(/[a-zа-яА-ЯёЁ ]/i)) return false;
        });
      },
      phone: function phone($input, options) {
        $input.inputmask(options);
      },
      range: function range($input, options) {
        engageRangeSlider($input, options);
      },
      date: function date($input, options) {
        console.log(options); // console.log('hello, i datepicker');

        $input.calendar(options);
      },
      numeric: function numeric($input) {
        $input.on('keypress', function (event) {
          var keyChar = global.getChar(event),
              max = $input.data('max'),
              value = $input.val();
          if (keyChar === null || !keyChar.match(/[0-9]/i)) return false;

          if (value >= max) {
            $input.val(max);
            return false;
          }
        });
      }
    },
    pluginOpts: {
      range: {},
      phone: {
        mask: '+7(999)-999-99-99'
      },
      date: {
        format: 'd.m.Y'
      }
    },
    validator: {
      options: {
        modules: 'security',
        showHelpOnFocus: false,
        addSuggestions: false,
        borderColorOnError: '',
        onValidate: function onValidate(result) {
          console.log(result);
        }
      },
      rules: {
        common: {
          'validation-error-msg': ' '
        },
        name: {
          'validation': 'required length',
          'validation-length': 'min2',
          'validation-error-msg': 'Укажите имя'
        },
        password: {
          'validation': 'required strength',
          'data-validation-strength': 2
        },
        email: {
          'validation': 'required email',
          'validation-error-msg': 'Укажите корректный e-mail'
        },
        phone: {
          'validation': 'required custom',
          'validation-regexp': '^\\+\\d{1}\\(\\d{3}\\)-\\d{3}-\\d{2}-\\d{2}$',
          'validation-error-msg': 'Укажите корректный телефон'
        },
        date: {
          'validation': 'required'
        },
        required: {
          'validation': 'required'
        }
      }
    },
    url: '/ajax/request_form.php',
    successCallback: function successCallback() {
      console.log('ok');
    },
    errorCallback: function errorCallback() {
      console.log('error');
    },
    removeLabels: false,
    async: true,
    watchDay: false
  };

  function init() {
    var self = this,
        active = global.classes.active;
    self.$element.attr('id', self.$element.attr('name'));
    $.each(self.options.structure.children, function (name, value) {
      self['$' + name] = self.$element.find('.' + value);
    });
    self.$input.not(':disabled').each(function (index, input) {
      var $input = $(input),
          type = $input.data('type') || $input.data('input-type');

      if (type && self.options.validator.rules[type]) {
        var validatorData = $.extend({}, self.options.validator.rules.common, self.options.validator.rules[type]);
        $.each(validatorData, function (name, value) {
          $input.attr('data-' + name, value);
        });
      }

      if (type && self.options.fn[type]) {
        self.options.fn[type]($input, self.options.pluginOpts[type] || undefined);
      }
    });
    self.$dayRadio.each(function (ind, el) {
      var $el = $(el),
          day = $el.attr('id').split('-').pop();
      self['$' + day + 'Radio'] = $el;
    });
    if (self.$dayRadio.length && self.$timeSlider.length && self.options.watchDay) $.proxy(watchDay, self)();
    if (self.options.removeLabels) $.proxy(removeLabels, self)();
    self.$select.on('change', function () {
      var $self = $(this),
          value = $self.val(),
          $group = $self.closest('.input-group'),
          $label = $group.find('.label'),
          hidden = global.classes.hidden;
      $self.validate();
      if (value && self.options.removeLabels) $label.addClass(hidden);
    });
    self.$select.on('click', function () {
      var $self = $(this),
          $group = $self.closest('.input-group');
      $group.toggleClass(active);
    });
    self.$options.on('click', '.item', function () {
      var $self = $(this),
          $group = $self.closest('.input-group'),
          $input = $group.find('.input'),
          value = $self.data('item');
      $input.val(value);
      $input.change();
      $group.removeClass(active);
    });
    global.autosize(self.$textarea);
    $.validate($.extend({}, {
      form: '#' + self.$element.attr('id')
    }, self.options.validator.options)); // self.$agreement.on('click', function () {
    // 	if(self.$agreement.prop('checked')) {
    // 		self.$agreement.removeClass('error');
    // 	}
    // });

    self.$submit.on('click', function () {
      if (self.$element.isValid({}, {
        onElementValidate: function onElementValidate(valid, $el, $form, errorMess) {
          console.log(valid);
          $el.validate();
        }
      }, false)) {
        // if(self.$agreement.length && !self.$agreement.prop('checked')) {
        // 	self.$agreement.addClass('error');
        // 	return false;
        // }
        $.proxy(sendForm, self)();
      }

      return false;
    });
  }

  function removeLabels() {
    var self = this,
        hidden = global.classes.hidden;
    self.$input.on('focus', function () {
      var $self = $(this),
          $group = $self.closest('.input-group'),
          $placeholder = $group.find('.placeholder');
      $placeholder.addClass(hidden);
    }).on('blur', function () {
      var $self = $(this),
          value = $self.val(),
          $group = $self.closest('.input-group'),
          $placeholder = $group.find('.placeholder');
      if (!value) $placeholder.removeClass(hidden);
    });
  }

  function engageRangeSlider(element, options) {
    var min = element.data('min'),
        max = element.data('max'),
        from = min,
        to = max,
        step = element.data('step'),
        arValue = element.val() ? element.val().split('-') : [],
        defaults = {
      type: 'double',
      min: min,
      max: max,
      from: from,
      to: to,
      step: step,
      hide_min_max: true,
      input_values_separator: '-',
      onFinish: function onFinish() {
        resetSliderInput();
      },
      onChange: function onChange() {
        resetSliderInput();
      },
      onStart: function onStart() {
        setTimeout(function () {
          resetSliderInput();
        }, 1000);
      },
      onUpdate: function onUpdate() {
        resetSliderInput();
      }
    };

    if (arValue.length > 1) {
      defaults.from = arValue[0];
      defaults.to = arValue[1];
    }

    function resetSliderInput() {
      if (element.val() === min + '-' + max) element.val('');
    }

    element.ionRangeSlider($.extend({}, defaults, options));
  }

  function sendForm() {
    var self = this;
    if (self.options.async) $.proxy(sendFormAsync, self)();else self.element.submit();
  }

  function sendFormAsync() {
    var self = this,
        url = self.options.url,
        successCallback = self.options.successCallback,
        errorCallback = self.options.errorCallback,
        data = new FormData(self.element);
    $.ajax({
      url: url,
      data: data,
      type: "POST",
      cache: false,
      contentType: false,
      processData: false,
      success: function success(response) {
        if (successCallback) successCallback(response);
      },
      error: function error() {
        if (errorCallback) {
          errorCallback();
        }
      }
    });
  }

  function watchDay() {
    var self = this,
        $todayRadio = self.$todayRadio,
        $tomorrowRadio = self.$tomorrowRadio,
        rangeSlider = self.$timeSlider.data('ionRangeSlider'),
        $todayLabel = $todayRadio.parent().find('.label[for="' + $todayRadio.attr('id') + '"]'),
        from = self.$timeSlider.data('min'),
        to = self.$timeSlider.data('max'),
        date,
        hour;

    function isWorkingHour(hour, to) {
      return hour < to;
    }

    function checkDay() {
      if (!isWorkingHour(date.getHours(), to)) {
        $tomorrowRadio.prop('checked', true);
        $todayLabel.addClass(window.classes.hidden);
      } else {
        $todayRadio.prop('checked', true);
        $todayLabel.removeClass(window.classes.hidden);
        rangeSlider.update({
          from_min: hour,
          from: hour
        });
      }
    }

    function updateTime() {
      date = new Date();
      hour = isWorkingHour(date.getHours(), to) ? date.getHours() + 1 : from;
      checkDay();
    }

    updateTime();
    setInterval(function () {
      updateTime();
    }, 1000 * 30);
    $todayRadio.on('click', function () {
      rangeSlider.update({
        from_min: hour,
        from: hour
      });
    });
    $tomorrowRadio.on('click', function () {
      rangeSlider.update({
        from_min: from,
        from: from
      });
    });
  }

  Formalizer.prototype.setUrl = function (url) {
    this.options.url = url;
  };

  $.fn.formalizer = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('formalizer');

      if (!data) {
        data = new Formalizer(this, _typeof(options) === 'object' && options);
        $this.data('formalizer', data);
      }
    });
  };

  $.fn.formalizer.Constructor = Formalizer;
})(window, document, jQuery);

/***/ }),
/* 8 */
/***/ (function(module, exports) {

(function gift_scroll(global, document, $) {
  var $pageGift = $('.page-gift');

  if ($pageGift.length) {
    $(window).scroll(function (e) {
      // Get the position of the location where the scroller starts.
      var scroller_anchor = $(".scroller_anchor").offset().top;
      var lastId,
          topMenu = $(".tabs--nav"),
          topMenuHeight = topMenu.outerHeight() + 15,
          // All list items
      menuItems = topMenu.find("a"),
          // Anchors corresponding to menu items
      scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));

        if (item.length) {
          return item;
        }
      }); // Get container scroll position

      var fromTop = $(this).scrollTop() + topMenuHeight; // Get id of current scroll item

      var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop) return this;
      }); // Get the id of the current element

      cur = cur[cur.length - 1];
      var id = cur && cur.length ? cur[0].id : "";

      if (lastId !== id) {
        lastId = id; // Set/remove active class

        console.log(id);
        menuItems.removeClass("active").parent().find('.tab-' + id).addClass("active"); // $('option[value="#' + id + '"]').attr('selected','selected');
      }
    });
    $('.tabs--nav a.tab[href^="#"]').click(function () {
      var idel = $(this).attr('href'),
          $el = $(idel);

      if (idel != '#') {
        $('html,body').animate({
          scrollTop: $el.offset().top - $('.tabs--nav').height()
        }, 'slow');
        return false;
      }
    });
    $('.card-images').each(function (ind, el) {
      $carusel_main = $(el).find('.carousel-main');
      $carusel_main.flickity({
        prevNextButtons: false,
        pageDots: false
      });
      var id = $carusel_main.attr('id'); // 2nd carousel, navigation

      $(el).find('.carousel-nav').flickity({
        asNavFor: '#' + id,
        //'.carousel-main',
        contain: true,
        pageDots: false,
        prevNextButtons: false
      });
    });
    $('#card-custom-sum').change(function () {
      console.log(this.value);
      var $ipg = $(this).closest('.input-group');

      if (this.value != '') {
        $ipg.addClass('filled');
      } else {
        $ipg.removeClass('filled');
      }
    }).change();
    $('.card--buy').click(function () {
      var $card = $(this).closest('.card-item'),
          id = $card.data('id'),
          name = $card.data('name'),
          cost = $card.data('cost');

      if (id == 6) {
        cost = $('#card-custom-sum').val();
      }

      return false;
    });
  }

  var $form = $('#card--order__form'),
      $formParent = $form.closest('.b-form');
  var minDate = new Date();
  minDate.setDate(minDate.getDate() + 1);

  function onResponse($form, $formParent, class_name, resp) {
    console.log($form);
    console.log($formParent);
    console.log(class_name);

    if (typeof resp !== 'undefined') {}

    $form.addClass('form--' + class_name);
  }

  $form.formalizer({
    url: '/ajax/events.php',
    // watchDay: true,
    successCallback: function successCallback(resp) {
      onResponse($form, $formParent, window.classes.success, resp);
    },
    errorCallback: function errorCallback() {
      onResponse($form, $formParent, window.classes.error);
    },
    pluginOpts: {
      range: {
        postfix: '.00',
        min_interval: 1
      },
      phone: {
        mask: '+9(999)-999-99-99'
      },
      date: {
        pickmeup: {
          // flat : true,
          // hide_on_select: false,
          select_year: false,
          min: minDate
        }
      }
    }
  });
  $('.card-item .card--buy').click(function () {
    var $this = $(this),
        $item = $this.closest('.card-item'),
        cost = $item.data('cost');

    if ($item.find('#card-custom-sum').length) {
      cost = $item.find('#card-custom-sum').val();
    }

    $('.basket').append(['<div class="basket--item" data-cost="', cost, '">', '<div>', $item.data('name'), '</div>', '<div>Номинал ', cost, '₽', '</div>', '<div>', '<div class="input--number__overlay">', '<span class="minuse"> — </span>', '<input class="input input--number" name="basket[', $item.data('id'), ']" value="1">', '<span class="pluse"> + </span>', '</div>', '</div>', '<div class="basket--item__summary summary">', cost, '₽', '</div>', '</div>'].join(''));
    change_count();
  });
  $('body').on('click', '.input--number__overlay span', function () {
    console.log(this);
    var $this = $(this),
        $overlay = $this.closest('.input--number__overlay'),
        $input = $overlay.find('input'),
        count = parseInt($input.val());

    if ($this.hasClass('pluse')) {
      count++;
      $input.val(count);
      $input.change();
    } else {
      if (count != 1) {
        count--;
        $input.val(count);
        $input.change();
      } else {
        $this.closest('.basket--item').remove();
      }
    }

    return false;
  });
  $('body').on('blur change keyup', '.input--number', function () {
    console.log(this.value);
    var count = this.value,
        $item = $(this).closest('.basket--item'),
        cost = $item.data('cost'),
        $summary = $item.find('.summary');
    $summary.html(cost * count + '₽');
    change_count();
  });

  var change_count = function change_count() {
    var $items = $form.find('.basket--item'),
        sum_basket = 0,
        $btn_submit = $form.find('.submit');
    $items.each(function (ind, item) {
      var $item = $(item),
          count = $item.find('.input').val(),
          cost = $item.data('cost'),
          sum_item = count * cost;
      sum_basket += sum_item;
    });
    $btn_submit.find('.cost').html(sum_basket + '₽');
  };

  change_count();
})(window, document, jQuery);

/***/ }),
/* 9 */
/***/ (function(module, exports) {

(function educationPage(global, document, $) {
  $('.edu--toc__list ol li a').click(function () {
    var idel = $(this).attr('href'),
        $el = $(idel);
    $('html,body').animate({
      scrollTop: $el.offset().top
    }, 'slow');
    return false;
  });
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  function getYouTubeInfo(vidid) {
    $.ajax({
      url: "http://gdata.youtube.com/feeds/api/videos/" + vidid + "?v=2&alt=json&orderby=published&prettyprint=true",
      dataType: "jsonp",
      success: function success(data) {
        parseresults(data);
      }
    });
  }

  function parseresults(data) {
    console.log(data);
  }

  function onPlayerReady(event) {
    console.log(event);
    event.target.playVideo();
  }

  var done = false;

  function onPlayerStateChange(event) {// if (event.data == YT.PlayerState.PLAYING && !done) {
    //   setTimeout(stopVideo, 6000);
    //   done = true;
    // }
  }

  function stopVideo() {// player.stopVideo();
  }

  var players = {};
  $('.less--item .video--link a').click(function () {
    var $this = $(this),
        href = $this.attr('href'),
        $item = $this.closest('.less--item'),
        $video_item = $item.find('.less--item__video'),
        $video_item__txt = $video_item.find('.less--item__video-txt'),
        $video_item__iframe = $video_item.find('.less--item__video-iframe'),
        $player = $video_item__iframe.find('.player');
    var youtube_id = ytID(href);
    $video_item.slideDown();
    $('html,body').animate({
      scrollTop: $video_item.offset().top
    }, 'slow'); // getYouTubeInfo(youtube_id);

    $video_item__txt.html('');
    console.log($player.prop("tagName"));

    if ($player.prop("tagName") == 'IFRAME') {
      player = players[$player.attr('id')];
      player.loadVideoById({
        'videoId': youtube_id,
        'startSeconds': 0
      });
    } else {
      player = new YT.Player($player.attr('id'), {
        height: '360',
        width: '640',
        videoId: youtube_id,
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
      players[$player.attr('id')] = player; //   $player.data('pl', player);
    } // var url = 'https://www.youtube.com/watch?v=' + youtube_id;
    // $.getJSON('https://noembed.com/embed', {format: 'json', url: url}, function(data) {
    //         //alert(data.title);
    //     console.log(data);


    $video_item__txt.append('<h2 class="h2">' + $this.data('title') + '</h2>'); // });

    return false;
  });
})(window, document, jQuery);

/***/ }),
/* 10 */
/***/ (function(module, exports) {

(function eventsPage(global, document, $) {
  var $page = $('.page-activity');

  if ($page.length) {
    var onResponse = function onResponse($form, $formParent, class_name, resp) {
      console.log($form);
      console.log($formParent);
      console.log(class_name);

      if (typeof resp !== 'undefined') {}

      $form.addClass('form--' + class_name);
    };

    var fl_options = {
      // options
      cellAlign: 'left',
      contain: false,
      // autoPlay: 10000,
      // freeScroll: true,
      wrapAround: true,
      prevNextButtons: false
    };
    $('.menu--gallery').flickity(fl_options);
    var $form = $('#events--booking__form'),
        $formParent = $form.closest('.b-form');
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    $form.formalizer({
      url: '/ajax/events.php',
      // watchDay: true,
      successCallback: function successCallback(resp) {
        onResponse($form, $formParent, window.classes.success, resp);
      },
      errorCallback: function errorCallback() {
        onResponse($form, $formParent, window.classes.error);
      },
      pluginOpts: {
        range: {
          postfix: '.00',
          min_interval: 1
        },
        phone: {
          mask: '+9(999)-999-99-99'
        },
        date: {
          pickmeup: {
            // flat : true,
            // hide_on_select: false,
            select_year: false,
            min: minDate
          }
        }
      }
    });
    $('input[name="count"], input[name="param[6]"], input[name="param[7]"]').on('change keyup', function () {
      var count = $form.find('input[name="count"]').val(),
          parmas6 = $form.find('input[name="param[6]"]:checked').closest('.radio-item').data('cost'),
          parmas7 = $form.find('input[name="param[7]"]:checked').closest('.radio-item').data('cost'),
          $order_price = $('#order--price'),
          sum_order = parseInt(count) * (parseFloat(parmas6) + parseFloat(parmas7));
      console.log(count);
      console.log(parmas6);
      console.log(parmas7);
      $order_price.html(sum_order + ' ₽');
    });
    $('input[name="count"]').change();
  }
})(window, document, jQuery);

/***/ })
/******/ ]);
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(8);


/***/ }),

/***/ 8:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function anchor(global, document, $) {
  function Anchor(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Anchor.prototype.defaults = {
    speed: 600,
    topOffset: 0
  };

  function init() {
    var self = this;
    self.href = self.$element.data('href') || self.$element.attr('href');
    self.$element.on('click', function (e) {
      e.preventDefault();
      self.$correspondingBlock = self.href ? $(self.href) : $('html');

      if (self.$correspondingBlock.length) {
        self.scrollToBlock(self.options.speed);
      }
    });
  }

  Anchor.prototype.setTopOffset = function (topOffset) {
    this.topOffset = topOffset;
  };

  Anchor.prototype.scrollToBlock = function (speed) {
    if (window.globalScroll) {
      var scrollBar = window.globalScroll,
          targetPosition = window.getOffsetRect(document.querySelector(this.href)).top,
          currentOffset = scrollBar.offset.y,
          position = targetPosition + currentOffset - this.options.topOffset;
      window.globalScroll.scrollTo(0, position, this.options.speed);
    } else {
      var $scrollBlock = $('body, html'),
          topOffset = this.topOffset || this.options.topOffset,
          scrollPosition = this.$correspondingBlock.offset().top - topOffset;
      $scrollBlock.animate({
        scrollTop: scrollPosition
      }, speed || this.options.speed);
    }
  };

  $.fn.anchor = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('anchor');

      if (!data) {
        data = new Anchor(this, _typeof(options) === 'object' && options);
        $this.data('anchor', data);
      }
    });
  };

  $.fn.anchor.Constructor = Anchor;
})(window, document, jQuery);

/***/ })

/******/ });
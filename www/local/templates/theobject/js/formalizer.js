/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 13);
/******/ })
/************************************************************************/
/******/ ({

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),

/***/ 3:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function formalizer(global, document, $, undefined) {
  function Formalizer(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Formalizer.prototype.defaults = {
    structure: {
      children: {
        input: 'input',
        select: 'input.select',
        dayRadio: 'input.day',
        file: 'input.file',
        rangeSlider: 'input.range-slider',
        timeSlider: 'input.range-slider.time-slider',
        agreement: 'input.agreement',
        textarea: 'input.textarea',
        options: 'input-group .options',
        submit: 'submit'
      }
    },
    fn: {
      name: function name($input) {
        $input.on('keypress', function (event) {
          var keyChar = global.getChar(event);
          if (keyChar === null || !keyChar.match(/[a-zÐ°-Ñ ]/i)) return false;
        });
      },
      phone: function phone($input, options) {
        $input.inputmask(options);
      },
      range: function range($input, options) {
        engageRangeSlider($input, options);
      },
      date: function date($input, options) {
        $input.calendar(options);
      },
      numeric: function numeric($input) {
        $input.on('keypress', function (event) {
          var keyChar = global.getChar(event),
              max = $input.data('max'),
              value = $input.val();
          if (keyChar === null || !keyChar.match(/[0-9]/i)) return false;

          if (value >= max) {
            $input.val(max);
            return false;
          }
        });
      }
    },
    pluginOpts: {
      range: {},
      phone: {
        mask: '+7(999)-999-99-99'
      },
      date: {
        format: 'd.m.Y'
      }
    },
    validator: {
      options: {
        modules: 'security',
        showHelpOnFocus: false,
        addSuggestions: false,
        borderColorOnError: ''
      },
      rules: {
        common: {
          'validation-error-msg': ' '
        },
        name: {
          'validation': 'required length',
          'validation-length': 'min2',
          'validation-error-msg': 'Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾ Ð·Ð°Ð¿Ð¾Ð»Ð½Ð¸Ñ‚ÑŒ'
        },
        password: {
          'validation': 'required strength',
          'data-validation-strength': 2
        },
        email: {
          'validation': 'required email',
          'validation-error-msg': 'Ð½ÐµÐºÐ¾Ñ€Ñ€ÐµÐºÑ‚Ð½Ñ‹Ð¹ Ñ„Ð¾Ñ€Ð¼Ð°Ñ‚'
        },
        phone: {
          'validation': 'required custom',
          'validation-regexp': '^\\+\\d{1}\\(\\d{3}\\)-\\d{3}-\\d{2}-\\d{2}$',
          'validation-error-msg': 'Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾ Ð·Ð°Ð¿Ð¾Ð»Ð½Ð¸Ñ‚ÑŒ'
        },
        date: {
          'validation': 'required'
        },
        required: {
          'validation': 'required'
        }
      }
    },
    url: '/ajax/request_form.php',
    successCallback: function successCallback() {
      console.log('ok');
    },
    errorCallback: function errorCallback() {
      console.log('error');
    },
    removeLabels: false,
    async: true,
    watchDay: false
  };

  function init() {
    var self = this,
        active = global.classes.active;
    self.$element.attr('id', self.$element.attr('name'));
    $.each(self.options.structure.children, function (name, value) {
      self['$' + name] = self.$element.find('.' + value);
    });
    self.$input.not(':disabled').each(function (index, input) {
      var $input = $(input),
          type = $input.data('type') || $input.data('input-type');

      if (type && self.options.validator.rules[type]) {
        var validatorData = $.extend({}, self.options.validator.rules.common, self.options.validator.rules[type]);
        $.each(validatorData, function (name, value) {
          $input.attr('data-' + name, value);
        });
      }

      if (type && self.options.fn[type]) {
        self.options.fn[type]($input, self.options.pluginOpts[type] || undefined);
      }
    });
    self.$dayRadio.each(function (ind, el) {
      var $el = $(el),
          day = $el.attr('id').split('-').pop();
      self['$' + day + 'Radio'] = $el;
    });
    if (self.$dayRadio.length && self.$timeSlider.length && self.options.watchDay) $.proxy(watchDay, self)();
    if (self.options.removeLabels) $.proxy(removeLabels, self)();
    self.$select.on('change', function () {
      var $self = $(this),
          value = $self.val(),
          $group = $self.closest('.input-group'),
          $label = $group.find('.label'),
          hidden = global.classes.hidden;
      $self.validate();
      if (value && self.options.removeLabels) $label.addClass(hidden);
    });
    self.$select.on('click', function () {
      var $self = $(this),
          $group = $self.closest('.input-group');
      $group.toggleClass(active);
    });
    self.$options.on('click', '.item', function () {
      var $self = $(this),
          $group = $self.closest('.input-group'),
          $input = $group.find('.input'),
          value = $self.data('item');
      $input.val(value);
      $input.change();
      $group.removeClass(active);
    });
    global.autosize(self.$textarea);
    $.validate($.extend({}, {
      form: '#' + self.$element.attr('id')
    }, self.options.validator.options));
    self.$agreement.on('click', function () {
      if (self.$agreement.prop('checked')) {
        self.$agreement.removeClass('error');
      }
    });
    self.$submit.on('click', function () {
      if (self.$element.isValid({}, {
        onElementValidate: function onElementValidate(valid, $el, $form, errorMess) {
          $el.validate();
        }
      }, false)) {
        if (self.$agreement.length && !self.$agreement.prop('checked')) {
          self.$agreement.addClass('error');
          return false;
        }

        $.proxy(sendForm, self)();
      }

      return false;
    });
  }

  function removeLabels() {
    var self = this,
        hidden = global.classes.hidden;
    self.$input.on('focus', function () {
      var $self = $(this),
          $group = $self.closest('.input-group'),
          $placeholder = $group.find('.placeholder');
      $placeholder.addClass(hidden);
    }).on('blur', function () {
      var $self = $(this),
          value = $self.val(),
          $group = $self.closest('.input-group'),
          $placeholder = $group.find('.placeholder');
      if (!value) $placeholder.removeClass(hidden);
    });
  }

  function engageRangeSlider(element, options) {
    var min = element.data('min'),
        max = element.data('max'),
        from = min,
        to = max,
        step = element.data('step'),
        arValue = element.val() ? element.val().split('-') : [],
        defaults = {
      type: 'double',
      min: min,
      max: max,
      from: from,
      to: to,
      step: step,
      hide_min_max: true,
      input_values_separator: '-',
      onFinish: function onFinish() {
        resetSliderInput();
      },
      onChange: function onChange() {
        resetSliderInput();
      },
      onStart: function onStart() {
        setTimeout(function () {
          resetSliderInput();
        }, 1000);
      },
      onUpdate: function onUpdate() {
        resetSliderInput();
      }
    };

    if (arValue.length > 1) {
      defaults.from = arValue[0];
      defaults.to = arValue[1];
    }

    function resetSliderInput() {
      if (element.val() === min + '-' + max) element.val('');
    }

    element.ionRangeSlider($.extend({}, defaults, options));
  }

  function sendForm() {
    var self = this;
    if (self.options.async) $.proxy(sendFormAsync, self)();else self.element.submit();
  }

  function sendFormAsync() {
    var self = this,
        url = self.options.url,
        successCallback = self.options.successCallback,
        errorCallback = self.options.errorCallback,
        data = new FormData(self.element);
    $.ajax({
      url: url,
      data: data,
      type: "POST",
      cache: false,
      contentType: false,
      processData: false,
      success: function success(response) {
        if (successCallback) successCallback(response);
      },
      error: function error() {
        if (errorCallback) {
          errorCallback();
        }
      }
    });
  }

  function watchDay() {
    var self = this,
        $todayRadio = self.$todayRadio,
        $tomorrowRadio = self.$tomorrowRadio,
        rangeSlider = self.$timeSlider.data('ionRangeSlider'),
        $todayLabel = $todayRadio.parent().find('.label[for="' + $todayRadio.attr('id') + '"]'),
        from = self.$timeSlider.data('min'),
        to = self.$timeSlider.data('max'),
        date,
        hour;

    function isWorkingHour(hour, to) {
      return hour < to;
    }

    function checkDay() {
      if (!isWorkingHour(date.getHours(), to)) {
        $tomorrowRadio.prop('checked', true);
        $todayLabel.addClass(window.classes.hidden);
      } else {
        $todayRadio.prop('checked', true);
        $todayLabel.removeClass(window.classes.hidden);
        rangeSlider.update({
          from_min: hour,
          from: hour
        });
      }
    }

    function updateTime() {
      date = new Date();
      hour = isWorkingHour(date.getHours(), to) ? date.getHours() + 1 : from;
      checkDay();
    }

    updateTime();
    setInterval(function () {
      updateTime();
    }, 1000 * 30);
    $todayRadio.on('click', function () {
      rangeSlider.update({
        from_min: hour,
        from: hour
      });
    });
    $tomorrowRadio.on('click', function () {
      rangeSlider.update({
        from_min: from,
        from: from
      });
    });
  }

  Formalizer.prototype.setUrl = function (url) {
    this.options.url = url;
  };

  $.fn.formalizer = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('formalizer');

      if (!data) {
        data = new Formalizer(this, _typeof(options) === 'object' && options);
        $this.data('formalizer', data);
      }
    });
  };

  $.fn.formalizer.Constructor = Formalizer;
})(window, document, jQuery);

/***/ })

/******/ });
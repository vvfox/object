/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 23);
/******/ })
/************************************************************************/
/******/ ({

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(24);


/***/ }),

/***/ 24:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function timeline(global, document, $) {
  function Timeline(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Timeline.prototype.defaults = {
    structure: {
      progressBar: '.timeline__progress',
      pointsBlock: '.timeline__points'
    },
    playEvent: 'play',
    stopEvent: 'stop',
    pointClass: 'point',
    loop: false
  };

  function init() {
    var self = this,
        $video = self.options.$video,
        playEvent = self.options.playEvent,
        stopEvent = self.options.stopEvent;
    self.$window = $(window);
    $.proxy(setStructure, self)();

    if (self.options.timelinePoints) {
      $.proxy(initPoints, self)();
    }

    $video.on(playEvent, function () {
      self.state = playEvent;
      self.requestId = window.requestAnimationFrame($.proxy(updateTimeline, self));
    }).on(stopEvent, function () {
      self.state = stopEvent;
    });
    self.$element.on('click', function (event) {
      var progress = $.proxy(getProgress, self, event)(),
          curTime = self.options.getDuration() * progress;
      self.options.setVideoTime(curTime);
      $.proxy(setVideoProgress, self, progress)();
    });
  }

  function initPoints() {
    var self = this,
        duration = self.options.getDuration(),
        pointClass = self.options.pointClass;
    $.each(self.options.timelinePoints, function (index, point) {
      var $point = $('<div class="' + pointClass + '">' + point.name + '</div>'),
          leftPos = 100 * (point.time / duration) + '%';
      $point.data('index', index);
      $point.css({
        left: leftPos
      });
      self.$pointsBlock.append($point);
    });
    self.$points = self.$pointsBlock.find('.' + pointClass);
    self.$points.on('click', function () {
      var $self = $(this),
          index = $self.data('index'),
          curTime = self.options.timelinePoints[index].time,
          duration = self.options.getDuration(),
          progress = curTime / duration;
      self.options.setVideoTime(curTime);
      $.proxy(setVideoProgress, self, progress)();
      return false;
    });
  }

  function getProgress(event) {
    var self = this,
        $timeline = self.$element,
        w,
        x;

    if (event) {
      w = $timeline.width();
      x = event.pageX - $timeline.offset().left;
    } else {
      w = self.options.getDuration();
      x = self.options.getCurrentTime();
    }

    return x / w;
  }

  function setVideoProgress(progress) {
    var self = this;

    if (!self.options.loop && progress === 1) {
      $.proxy(stopProgress, self)();
      if (self.options.onStop) self.options.onStop();
    }

    self.$progressBar.css({
      width: 100 * progress + '%'
    });
  }

  function updateTimeline() {
    var self = this;

    if (self.state === self.options.stopEvent) {
      $.proxy(stopProgress, self)();
    } else {
      var progress = $.proxy(getProgress, self)();
      $.proxy(setVideoProgress, self, progress)();
    }

    window.requestAnimationFrame($.proxy(updateTimeline, self));
  }

  function stopProgress() {
    var self = this;

    if (self.requestId) {
      cancelAnimationFrame(self.requestId);
      $.proxy(setVideoProgress, self, 0)();
    }
  }

  function setStructure() {
    var self = this,
        structure = self.options.structure;
    $.each(structure, function (name, value) {
      self['$' + name] = self.$element.find(value);
    });
  }

  Timeline.prototype.destroy = function () {
    var self = this,
        $video = self.options.$video,
        playEvent = self.options.playEvent;
    $.proxy(stopProgress, self)();
    self.$element.data('timeline', null);
    $video.off(playEvent, updateTimeline);
  };

  $.fn.timeline = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('timeline');

      if (!data) {
        data = new Timeline(this, _typeof(options) === 'object' && options);
        $this.data('timeline', data);
      }
    });
  };

  $.fn.timeline.Constructor = Timeline;
})(window, document, jQuery);

/***/ })

/******/ });
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 15);
/******/ })
/************************************************************************/
/******/ ({

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(16);


/***/ }),

/***/ 16:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function gallery(global, document, $) {
  function Gallery(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Gallery.prototype.defaults = {
    structure: {
      switchContainer: '.gallery__switch',
      switchTab: '.switch__tab',
      tabsContainer: '.gallery__tabs',
      tab: '.gallery__tab'
    },
    dataTabKey: 'tab',
    autoData: true,
    disabledClass: '.disabled',
    autoPlay: false,
    autoPlayInterval: 4000,
    switchOnClick: true,
    switchOnHover: false
  };

  function init() {
    $.proxy(setStructure, this)();
    if (this.options.autoData) $.proxy(setData, this)();
    this.currentTabNum = $.proxy(getCurrentTabNum, this)();
    this.tabCount = this.$tabs.length;
    if (this.options.autoPlay) $.proxy(startAnimation, this)();
    if (this.options.switchOnClick) $.proxy(setSwitchTabHandler, this, 'click')();
    if (this.options.switchOnHover) $.proxy(setSwitchTabHandler, this, 'mouseenter')();
  }

  function setStructure() {
    var self = this,
        $gallery = self.$element,
        structure = self.options.structure;
    self.$switch = $gallery.children(structure.switchContainer);
    self.$switchTabs = self.$switch.find(structure.switchTab);
    self.$tabsContainer = $gallery.children(structure.tabsContainer);
    self.$tabs = self.$tabsContainer.children(structure.tab);
  }

  function setData() {
    var self = this,
        key = self.options.dataTabKey;
    self.$switchTabs.each(function (ind, el) {
      var $el = $(el);
      $el.data(key, ind + 1);
    });
    self.$tabs.each(function (ind, el) {
      var $el = $(el);
      $el.data(key, ind + 1);
    });
  }

  function switchTab(activeTab) {
    var self = this,
        nextTabNum = self.currentTabNum < self.tabCount ? self.currentTabNum + 1 : 1,
        tab = activeTab || nextTabNum;
    self.currentTabNum = tab;
    self.$switchTabs.each(function (ind, el) {
      $.proxy(activateTab, self, $(el), tab)();
    });
    self.$tabs.each(function (ind, el) {
      $.proxy(activateTab, self, $(el), tab)();
    });
  }

  function activateTab($tab, activeTab) {
    var active = window.classes.active,
        key = this.options.dataTabKey;
    if ($tab.data(key) === activeTab) $tab.addClass(active);else $tab.removeClass(active);
  }

  function getTab(tab) {
    var self = this,
        $tab = undefined;
    self.$tabs.each(function (ind, el) {
      var $el = $(el);

      if ($el.data(self.options.dataTabKey) === tab) {
        $tab = $el;
        return false;
      }
    });
    return $tab;
  }

  function getCurrentTabNum() {
    var self = this,
        tabNum = undefined;
    self.$tabs.each(function (ind, el) {
      var $el = $(el);

      if ($el.hasClass(window.classes.active)) {
        tabNum = $el.data(self.options.dataTabKey);
        return false;
      }
    });
    return tabNum;
  }

  function setSwitchTabHandler(event) {
    var self = this,
        switchTabClass = self.options.structure.switchTab,
        disabled = self.options.disabled;
    self.$element.on(event, switchTabClass + ':not(' + disabled + ')', function () {
      if (self.animationStarted) $.proxy(stopAnimation, self)();
      $.proxy(switchTabHandler, this, self)();
    }).on('mouseleave', switchTabClass + ':not(' + disabled + ')', function () {
      if (self.options.autoPlay) $.proxy(startAnimation, self)();
    });
  }

  function switchTabHandler(mainContext) {
    var $tab = $(this),
        tab = $tab.data(mainContext.options.dataTabKey);
    $.proxy(switchTab, mainContext, tab)();
    mainContext.$element.trigger('tabChanged', [tab, $.proxy(getTab, mainContext, tab)()]);
  }

  function startAnimation() {
    var self = this;

    if (!self.animationStarted) {
      self.intervalId = setInterval($.proxy(switchTab, self), self.options.autoPlayInterval);
      self.animationStarted = true;
    }
  }

  function stopAnimation() {
    var self = this;
    clearInterval(self.intervalId);
    self.animationStarted = false;
  }

  Gallery.prototype.switchTab = function (tabNum) {
    $.proxy(switchTab, this, tabNum)();
  };

  $.fn.gallery = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('gallery');

      if (!data) {
        data = new Gallery(this, _typeof(options) === 'object' && options);
        $this.data('gallery', data);
      }
    });
  };

  $.fn.gallery.Constructor = Gallery;
})(window, document, jQuery);

/***/ })

/******/ });
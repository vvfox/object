/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 28);
/******/ })
/************************************************************************/
/******/ ({

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(29);


/***/ }),

/***/ 29:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function youTubeVideo(global, document, $, undefined) {
  function YouTubeVideo(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  YouTubeVideo.prototype.defaults = {
    structure: {
      videoWrapper: '.video-over',
      player: '.player',
      play: '.btn-play',
      fullScreen: '.control--full-screen',
      timeline: '.b-timeline'
    },
    ytOptions: {
      width: 640,
      height: 360,
      playerVars: {
        controls: 0,
        enablejsapi: 1,
        origin: 'http://localhost:3000',
        showinfo: 0,
        modestbranding: 1,
        rel: 0
      },
      events: {}
    },
    autoLoad: true,
    playInFullSize: true,
    popup: '.b-popup--video',
    onStop: function onStop() {
      $.magnificPopup.close();
    }
  };

  function init() {
    var self = this;
    self.$window = $(window);
    self.playerState = 'stop';
    self.init = false;
    $.proxy(setStructure, self)();
    self.$popup = $(self.options.popup);
    $.proxy(setPlayerId, self)();
    $.proxy(setVideoId, self)();
    self.$window.on(window.events.ytReady, $.proxy(onApiReady, self));
    self.$play.on('click', $.proxy(onPlay, self));
    self.$fullScreen.on('click', function () {
      if (!window.isFullScreen()) {
        window.enterFullScreen(self.$element.get(0));
      } else {
        window.exitFullScreen();
      }

      return false;
    });
  }

  function setStructure() {
    var self = this,
        structure = self.options.structure;
    $.each(structure, function (name, value) {
      self['$' + name] = self.$element.find(value);
    });
  }

  function setPlayerId() {
    this.playerId = this.$player.attr('id');
  }

  function setVideoId() {
    this.videoId = this.$player.data('video');
  }

  function onApiReady() {
    var self = this;
    if (this.options.autoLoad) $.proxy(loadPlayer, self)();
  }

  function loadPlayer() {
    var self = this,
        ytOptions = self.options.ytOptions;
    ytOptions.videoId = self.videoId;
    ytOptions.events.onReady = $.proxy(onPlayerReady, self);
    ytOptions.events.onStateChange = $.proxy(onStateChange, self);
    self.player = new YT.Player(self.playerId, ytOptions);
  }

  function onPlayerReady() {
    var self = this;
    self.playerReady = true;
    $.proxy(setStructure, self)();
    self.$window.trigger('playerReady');
  }

  function onPlay() {
    var self = this;
    if (self.options.playInFullSize) $.proxy(openFullScreen, self)();else $.proxy(playVideo, self)();
    return false;
  }

  function playVideo() {
    var self = this,
        player = self.player,
        playerState = self.playerState;

    if (self.$timeline.length) {
      self.$timeline.timeline({
        $video: self.$element,
        playEvent: 'videoPlaying',
        getDuration: function getDuration() {
          return player.getDuration();
        },
        getCurrentTime: function getCurrentTime() {
          return player.getCurrentTime();
        },
        setVideoTime: function setVideoTime(curTime) {
          player.seekTo(curTime, true);
        },
        onStop: self.options.onStop
      });
    }

    if (!self.init) {
      self.init = true;
      self.$element.trigger('initialized');
    }

    if (playerState === 'stop' || playerState === 'pause') {
      self.playVideo();
    } else {
      self.pauseVideo();
    }
  }

  function openFullScreen() {
    var self = this,
        $videoPopup = self.$popup,
        $player = self.$player,
        play = $.proxy(playVideo, self);
    $videoPopup.append($player);
    $.magnificPopup.open({
      items: {
        src: $videoPopup,
        type: 'inline'
      },
      mainClass: 'mfp-custom mfp-full-size mfp-video',
      fixedContentPos: true,
      callbacks: {
        open: function open() {
          self.$window.on('playerReady', play);
          window.enterFullScreen($videoPopup.get(0));
        },
        close: function close() {
          self.$window.off('playerReady', play);
          self.$videoWrapper.append($player);
          self.$timeline.data('timeline').destroy();
        }
      }
    });
  }

  function onStateChange(event) {
    var self = this,
        statusCode = event.data;

    if (statusCode === YT.PlayerState.ENDED) {
      self.stopVideo();
    }
  }

  YouTubeVideo.prototype.loadPlayer = function () {
    $.proxy(loadPlayer, this)();
  };

  YouTubeVideo.prototype.playVideo = function () {
    this.player.playVideo();
    this.$element.trigger('videoPlaying');
    this.playerState = 'play';
  };

  YouTubeVideo.prototype.pauseVideo = function () {
    this.player.pauseVideo();
    this.playerState = 'pause';
    this.$element.trigger('videoPaused');
  };

  YouTubeVideo.prototype.stopVideo = function () {
    this.init = false;
    this.playerState = 'stop';
    this.$element.trigger('videoStopped');
  };

  $.fn.youTubeVideo = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('youTubeVideo');

      if (!data) {
        data = new YouTubeVideo(this, _typeof(options) === 'object' && options);
        $this.data('youTubeVideo', data);
      }
    });
  };

  $.fn.youTubeVideo.Constructor = YouTubeVideo;
})(window, document, jQuery);

/***/ })

/******/ });
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function calendar(global, document, $, pickmeup) {
  pickmeup.defaults.locales['ru'] = {
    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
  };

  function Calendar(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  Calendar.prototype.defaults = {
    pickmeup: {
      hide_on_select: true,
      class_name: 'calendar',
      locale: 'ru',
      format: 'd.m.Y'
    },
    custom: {
      append: true,
      disablePastDays: false,
      minDay: getMinDay()
    }
  };

  Calendar.prototype.getDate = function (formatted) {
    var self = this,
        element = self.element;
    return pickmeup(element).get_date(formatted);
  };

  Calendar.prototype.setDate = function (date) {
    var self = this,
        element = self.element;
    pickmeup(element).set_date(date);
    return self;
  };

  Calendar.prototype.refresh = function (opts) {
    var self = this;
    self.options = $.extend(true, {}, self.options, opts);
    $.proxy(destroy, self)();
    $.proxy(init, self)();
  };

  function destroy() {
    pickmeup(this.element).destroy();
  }

  function init() {
    var self = this,
        element = self.element,
        $element = self.$element,
        calOptions = self.options.pickmeup,
        minDay = self.options.custom.minDay;
    if (self.options.custom.disablePastDays) calOptions = $.extend({}, self.options.pickmeup, {
      min: minDay,
      render: disablePastDays(minDay)
    });
    pickmeup(element, calOptions);
    if (self.options.custom.append) $element.parent().append($(element.__pickmeup.element));
    $element.on('pickmeup-change', function () {
      $element.trigger('change');
    });
  }

  function disablePastDays(day) {
    return function (date) {
      if (date < day) {
        return {
          disabled: true,
          class_name: 'date-in-past'
        };
      }

      return {};
    };
  }

  function getMinDay() {
    var now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }

  $.fn.calendar = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('calendar');

      if (!data) {
        data = new Calendar(this, _typeof(options) === 'object' && options);
        $this.data('calendar', data);
      }
    });
  };

  $.fn.calendar.Constructor = Calendar;
})(window, document, jQuery, pickmeup);

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(10);


/***/ })

/******/ });
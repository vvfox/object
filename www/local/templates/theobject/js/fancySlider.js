/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(12);


/***/ }),

/***/ 12:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function fancySlider(global, document, $, undefined) {
  function FancySlider(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    init = init.bind(this);
    setStructure = setStructure.bind(this);
    initItems = initItems.bind(this);
    go = go.bind(this);
    goPrev = goPrev.bind(this);
    goNext = goNext.bind(this);
    setClass = setClass.bind(this);
    setCurrent = setCurrent.bind(this);
    switchPager = switchPager.bind(this);
    animatePager = animatePager.bind(this);
    setLoop = setLoop.bind(this);
    clearLoop = clearLoop.bind(this);
    engageSwipe = engageSwipe.bind(this);
    onSwipe = onSwipe.bind(this);
    init();
  }

  FancySlider.prototype.defaults = {
    structure: {
      items: '.item',
      imgContainer: '.b-img',
      img: '.img',
      pager: '.b-pager',
      pagerCur: '.b-pager .num--cur',
      pagerNext: '.b-pager .num--next'
    },
    directions: {
      prev: 'prev',
      next: 'next'
    },
    interval: 5000,
    loop: true,
    activeClass: window.classes.active,
    pagerHoldClass: window.classes.hold,
    currentClass: 'current'
  };

  function init() {
    var self = this,
        $window = $(window),
        nextKeyCodes = [13, 32, 34, 39, 40],
        prevKeyCodes = [33, 37, 38];
    setStructure();
    initItems();
    self.curItemNum = 0;
    self.itemCount = self.$items.length;
    self.isOnHold = false;
    self.$pagerCur.find('value').html('0' + (self.curItemNum + 1));
    self.$pagerNext.find('value').html('0' + (self.curItemNum + 2));
    $window.on('keydown', function (event) {
      if (!self.isOnHold) {
        var keyCode = event.which,
            direction = self.options.directions.next;
        if (nextKeyCodes.indexOf(keyCode) !== -1) goNext();

        if (prevKeyCodes.indexOf(keyCode) !== -1) {
          goPrev();
          direction = self.options.directions.prev;
        }

        if (self.options.loop) {
          setLoop(direction);
        }
      }
    }).on('resize', function () {
      self.$img.css({
        height: self.$imgContainer.height(),
        width: self.$imgContainer.width()
      });
    }).resize();

    if (!window.mediaChecker.isTablet()) {
      self.$element.on('mouseenter', function () {
        clearLoop();
      }).on('mouseleave', function () {
        if (self.options.loop) {
          setLoop();
        }
      });

      if (self.options.loop) {
        setLoop();
      }
    }

    if (window.mediaChecker.isTablet()) {
      engageSwipe();
    }
  }

  function setStructure() {
    var self = this,
        structure = self.options.structure;
    $.each(structure, function (name, value) {
      self['$' + name] = self.$element.find(value);
    });
  }

  function initItems() {
    var self = this;
    self.$items.each(function (index, item) {
      var $item = $(item);

      if (index === 0) {
        $item.addClass(self.options.activeClass + ' ' + self.options.currentClass);
      }

      $item.data('index', index);
    });
  }

  function goPrev() {
    go(this.options.directions.prev);
  }

  function goNext() {
    go(this.options.directions.next);
  }

  function go(direction) {
    var self = this,
        curItemNum = self.curItemNum,
        nextItemNum,
        actualItemNum,
        actualFuncFalse;
    self.isOnHold = true;

    if (direction === self.options.directions.prev) {
      nextItemNum = curItemNum - 1;
      actualItemNum = nextItemNum >= 0 ? nextItemNum : self.itemCount + nextItemNum;
      self.$element.addClass(self.options.directions.prev);
      self.$element.removeClass(self.options.directions.next);

      actualFuncFalse = function actualFuncFalse($item, className) {
        $item.removeClass(className);
      };
    }

    if (direction === self.options.directions.next) {
      nextItemNum = curItemNum + 1;
      actualItemNum = nextItemNum % self.itemCount;
      self.$element.removeClass(self.options.directions.prev);
      self.$element.addClass(self.options.directions.next);

      actualFuncFalse = function actualFuncFalse($item, className) {
        setTimeout(function () {
          $item.removeClass(className);
        }, 1000);
      };
    }

    self.curItemNum = actualItemNum;
    setClass(self.options.activeClass, actualItemNum, function ($item, className) {
      $item.addClass(className);
    }, actualFuncFalse);
    setCurrent();
    animatePager(actualItemNum);
    setTimeout(function () {
      self.isOnHold = false;
    }, 1000);
  }

  function setCurrent() {
    var self = this,
        className = self.options.currentClass;
    setClass(className, self.curItemNum, function ($item, className) {
      $item.addClass(className);
    }, function ($item, className) {
      $item.removeClass(className);
    });
  }

  function setClass(className, index, funcTrue, funcFalse) {
    var self = this;
    self.$items.each(function (ind, item) {
      var $item = $(item),
          itemIndex = $item.data('index');

      if (itemIndex === index) {
        funcTrue($item, className);
      } else if (funcFalse) {
        funcFalse($item, className);
      }
    });
  }

  function switchPager() {
    var self = this;
    self.$pagerCur.toggleClass(self.options.activeClass);
    self.$pagerNext.toggleClass(self.options.activeClass);
  }

  function animatePager(nextItemNum) {
    var self = this,
        nextItemNumStr = Math.floor(nextItemNum / 10) === 0 ? '0' + (nextItemNum + 1) : nextItemNum + 1;
    self.$pager.removeClass(self.options.pagerHoldClass);
    self.$pagerNext.find('.value').html(nextItemNumStr);
    switchPager();
    setTimeout(function () {
      self.$pager.addClass(self.options.pagerHoldClass);
      self.$pagerCur.find('.value').html(nextItemNumStr);
      switchPager();
    }, 1000);
  }

  function setLoop(direction) {
    var self = this,
        curDirection = direction || self.options.directions.next;
    clearLoop();
    self.intervalId = setInterval(function () {
      if (curDirection === self.options.directions.next) goNext();
      if (curDirection === self.options.directions.prev) goPrev();
    }, self.options.interval);
  }

  function clearLoop() {
    var self = this;
    if (self.intervalId) clearInterval(self.intervalId);
  }

  function engageSwipe() {
    var self = this,
        swiper = new Hammer(self.element);
    swiper.on('swipeleft', function () {
      onSwipe(self.options.directions.next);
    }).on('swiperight', function () {
      onSwipe(self.options.directions.prev);
    });
  }

  function onSwipe(direction) {
    var self = this;

    if (!self.isOnHold) {
      if (direction === self.options.directions.next) {
        goNext();
      }

      if (direction === self.options.directions.prev) goPrev();

      if (self.options.loop && !window.mediaChecker.isTablet()) {
        setLoop(direction);
      }
    }
  }

  $.fn.fancySlider = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('fancySlider');

      if (!data) {
        data = new FancySlider(this, _typeof(options) === 'object' && options);
        $this.data('fancySlider', data);
      }
    });
  };

  $.fn.fancySlider.Constructor = FancySlider;
})(window, document, jQuery);

/***/ })

/******/ });
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 26);
/******/ })
/************************************************************************/
/******/ ({

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(27);


/***/ }),

/***/ 27:
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function videoCover(global, document, $) {
  function VideoCover(element, options) {
    this.options = $.extend(true, {}, this.defaults, options);
    this.element = element;
    this.$element = $(element);
    $.proxy(init, this)();
  }

  VideoCover.prototype.defaults = {
    ratio: 16 / 9
  };

  function init() {
    var self = this;
    self.$parent = self.$element.parent();
    $(window).on('resize', function () {
      setTimeout(function () {
        self.resize();
      }, 300);
    }).resize();
  }

  function getWidth() {
    var videoContainerHeight = this.$parent.height(),
        videoContainerWidth = this.$parent.width(),
        ratio = this.options.ratio,
        parentRatio = videoContainerWidth / videoContainerHeight;
    return Math.ceil(parentRatio > ratio ? videoContainerWidth : videoContainerHeight * ratio);
  }

  function onResize() {
    var self = this,
        width = $.proxy(getWidth, self)(),
        height = width / self.options.ratio;
    self.$element.css({
      width: width,
      height: height
    });
  }

  VideoCover.prototype.resize = function () {
    $.proxy(onResize, this)();
  };

  $.fn.videoCover = function (options) {
    for (var _len = arguments.length, params = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      params[_key - 1] = arguments[_key];
    }

    this.each(function () {
      var $this = $(this),
          data = $this.data('videoCover');

      if ((!options || _typeof(options) === 'object') && !data) {
        data = new VideoCover(this, options);
        $this.data('videoCover', data);
      } else if (typeof options === 'string' && data && typeof data[options] === 'function') {
        data[options].apply(data, params);
      } else {
        throw 'videoCover: invalid call';
      }
    });
  };

  $.fn.videoCover.Constructor = VideoCover;
})(window, document, jQuery);

/***/ })

/******/ });
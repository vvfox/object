<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
	$dir = $APPLICATION->GetCurDir();
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/style.css"); 
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor.js"); 
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script.js"); 
	?><!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 	
	</head>
	<body>
	<div class="page page-<?=str_replace('/','',$dir)?>">
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<div class="header">
        <div class="header--overlay">
			<div class="wrapper">
				<nav>
					<a href="<?=SITE_DIR?>" class="logo"><img src="/local/templates/theobject/svg/logo.svg"/></a>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "tmp_mainmenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(0 => "",),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					);?>
             <div class="cabinet-link">Личный кабинет</div>
			 <?
        // включаемая область для раздела
        $APPLICATION->IncludeFile('/local/include/phone.php', Array(), Array(
            "MODE"      => "html",                                           // будет редактировать в веб-редакторе
            "NAME"      => "Бронирование стрельбы",      // текст всплывающей подсказки на иконке
            "TEMPLATE"  => "section_include_template.php"                    // имя шаблона для нового файла
            ));
        ?><div class="btn--menu"></div>
        </nav>
     </div>
    </div>
</div>
	
						
<div class="card--order__overlay">
    <div class="b-from">
        <form id="card--order__form">
            <div class="card--order__heeader">
                <h2 class="h2">Заказ</h2>
                <div class="card--order__typecard-select">
                    <div class="card--order__typecard">
                        <input type="radio" name="card--order__typecard" id="card--order__typecard_1" class="input" checked="checked" >
                        <label for="card--order__typecard_1" class="label">Электронная карта</label>
                    </div>
                    <div class="card--order__typecard">
                        <input type="radio" name="card--order__typecard" id="card--order__typecard_2" class="input" >
                        <label for="card--order__typecard_2" class="label">Физическая карта</label>
                    </div>
                </div>
            </div>

            <div class="basket">


            </div>


            <div class="select--delivery">

            </div>
            <div class="select--payment">
                
            </div>
   



    <div class="form--inputs">
        <div class="form--group">
            <div class="input-group">
                <input type="text" name="name" id="card--order__name" class="input" data-type="name">
                <label for="card--order__name" class="label">Имя</label>
            </div>
        </div>
        <div class="form--group">
            <div class="input-group">
                <input type="text" name="phone" id="card--order__phone" class="input" data-type="phone" >
                <label for="card--order__phone" class="label">Телефон</label> 
            </div>
        </div>
        <div class="form--group">
            <div class="input-group">
                <input type="text" name="email" id="card--order__email" class="input" data-type="email" >
                <label for="card--order__email" class="label">Электронная почта</label> 
            </div>
        </div>

    </div>

    <div class="basket--footer">

    <button class="btn btn--red submit">
        <div class="cost"></div>
        <div class="txt">
            Оплатить
        </div>
    </button>
    <p class="p">Нажимая кнопку "Оформить", вы соглашаетесь с тем, что ознакомились с условиями публичной оферты, политикой конфиденциальности и даёте согласие на обработку персональных данных.</p>


    </div>

    </form>
    </div>
</div>
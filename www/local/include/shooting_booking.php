<div class="shooting--popup">
    <div class="shooting--popup__shadows"></div>
        <div class="shooting--popup__body">
            <div class="shooting--popup__wrapper">
                <h3 class="h3">Запись на курс «<span class="shooting--popup__title"></span>»</h3>
            </div>
            <div class="b-form">
                <form action="" id="shooting--booking__form">

                <input type="hidden" name="product_id" />
                <input type="hidden" name="price" />

                    <div class="shooting--booking__step shooting--booking__step1">
                        <div class="shooting--popup__wrapper">
                            <div class="tabs">
                                <a class="tab tab-time" href="#shooting--booking__time">
                                    <div class="title">Дата и время</div>
                                    <div class="tooltip">&nbsp;</div>
                                </a>

                                <a class="tab tab-contact" href="#shooting--booking__contacts">
                                    <div class="title">Контакты</div> 
                                </a>
                            </div>
                        </div>

                        <div class="tab-container">
                            <div class="tab-content" id="shooting--booking__time">
                                <div class="form-section">
                                    <div class="shooting--popup__wrapper">
                                        <div class="form--group">
                                            <input class="input shooting--booking__calendar-input calendar" name="date" type="text" data-type="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-section">
                                    <div class="shooting--popup__wrapper">
                                        <div class="time-today">
                                            <? for($n=9; $n<= 22; $n++){?>
                                                <div class="chekbox-group">
                                                    <input type="radio" name="time" id="time-<?=$n?>" value="<?=($n<10?'0':'').$n?>:00" <?=$n==9?'':'';?>/>
                                                    <label for="time-<?=$n?>"><?=($n<10?'0':'').$n?>:00</label>
                                                </div>
                                            <?}?>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content" id="shooting--booking__contacts">
                                <div class="form-section">
                                <div class="shooting--popup__wrapper">
                                    <div class="form--group">
                                        <div class="input-group">
                                            <div class="">
                                                <input type="text" name="phone" id="shooting--booking__phone" class="input" data-type="phone">
                                                <label for="shooting--booking__phone" class="label">Телефон</label> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form--group">
                                        <div class="input-group">
                                            <div class="">
                                                <input type="text" name="name" id="shooting--booking__name" class="input" data-type="name">
                                                <label for="shooting--booking__name" class="label">Имя</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="shooting--popup__wrapper">
                                    <div class="form-summary">
                                        <div class="summary--wrap">
                                            <div class="shooting--count shooting--line">
                                                <div class="name">Стрелки</div>
                                                    <div class="value">
                                                        <?for($n=1;$n<5;$n++){?>
                                                            <div class="chekbox-group">
                                                                <input type="radio" name="count_shooters" id="count_shooters-<?=$n?>" value="<?=$n?>" <?=$n==1?'checked':'';?>/>
                                                                <label for="count_shooters-<?=$n?>"><?=$n?></label>
                                                            </div>
                                                        <?}?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-section">
                                                <div class="summary--wrap">
                                                    <div class="shooting--line">
                                                        <div class="name">К оплате</div>
                                                        <div class="value sum-value">

                                                        </div>


                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="summary--wrap">
                                                <button  type="submit" class="btn btn--red submit" >Оплата картой сейчас</button>
                                                <p class="small">Нажимая кнопку «Оплатить картой сейчас» или «Оплатить в клубе», я соглашаюсь с условиями публичной оферты и правилами, а также подтверждаю, что все участники тренировки совершеннолетние граждане РФ.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="shooting--booking__step shooting--booking__step2">
                    Для подтверждения заказа введите код из СМС
                </div>
            </div>
            <div class="shooting--booking__step shooting--booking__step3">
                Бронирование подтверждено.Ждем вас 15 сентября в 19:00.
                Номер заказа: 1260
                Не забудьте взять паспорт
            </div>
        </div>
    </div>
</div>
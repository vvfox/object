<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подарки");
?>
<div class="section section--main">

<div class="wrapper">
    <h1 class="h1">Дарите уникальные впечатления от знакомства с оружием</h1>
    <p class="lid">Подарите курс стрельбы из огнестрельного оружия в крупнейшем крытом тире.</p>
</div>

</div>

<section class="section section--nav">

<div class="tabs--nav">
    <div class="wrapper">
    <a class="tab tab-cards-3" href="#cards-3">Базовые курсы</a>
    <a class="tab tab-card-el-6" href="#card-el-6">С любой суммой на счету</a>
    <a class="tab tab-card-el-7" href="#card-el-7">Карта «Прошутер»</a>

    </div>
    
</div>
<div class="scroller_anchor"></div>


</section>

<section class="section scroller">
<?
global $arrFilterSection;
$arrFilterSection = ['SECTION_ID'=>2];

?><?$APPLICATION->IncludeComponent("bitrix:catalog.sections.top", "tmp_cards", Array(
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
		"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
		"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
		"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
		"ELEMENT_COUNT" => "9",	// Максимальное количество элементов, выводимых в каждом разделе
		"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
		"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
		"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
		"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
		"FILTER_NAME" => "arrFilterSection",	// Имя массива со значениями фильтра для фильтрации элементов
		"HIDE_NOT_AVAILABLE" => "N",	// Не отображать недоступные товары
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "content",	// Тип инфоблока
		"LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы
		"PRICE_CODE" => array(	// Тип цены
			0 => "base",
		),
		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
		"PRODUCT_PROPERTIES" => "",	// Характеристики товара
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"PROPERTY_CODE" => array(	// Свойства
			0 => "css",
			1 => "images",
		),
		"SECTION_COUNT" => "20",	// Максимальное количество выводимых разделов
		"SECTION_FIELDS" => array(	// Поля разделов
			"ID",
"CODE",
"EXTERNAL_ID",
"IBLOCK_ID",
"IBLOCK_SECTION_ID",
"TIMESTAMP_X",
"SORT",
"NAME",
"ACTIVE",
"GLOBAL_ACTIVE",
"PICTURE",
"DESCRIPTION",
"DESCRIPTION_TYPE",
"LEFT_MARGIN",
"RIGHT_MARGIN",
"DEPTH_LEVEL ",
"SEARCHABLE_CONTENT",
"SECTION_PAGE_URL",
"MODIFIED_BY",
"DATE_CREATE",
"CREATED_BY",
"DETAIL_PICTURE"
		),
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
		"SECTION_SORT_FIELD" => "sort",	// По какому полю сортируем разделы
		"SECTION_SORT_ORDER" => "asc",	// Порядок сортировки разделов
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "",
			1 => "",
		),
		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
		"USE_MAIN_ELEMENT_SECTION" => "Y",	// Использовать основной раздел для показа элемента
		"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
		"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
</section>
<section class="section">
    <div class="wrapper">
        <div class="card-text">


        <div class="card-text-col">
            <div class="cart-text-img"><img src="./../local/templates/theobject/img/card/box.png" /></div>
            <div class="cart-text-txt">
                <h3 class="h3">Подарочная карта</h3>
                <p class="p">Выбирайте курс, а мы доставим карту по указанному адресу, в дизайнерской упаковке.</p>

            </div>
        </div>
        <div class="card-text-col">
            <div class="cart-text-img"><img src="./../local/templates/theobject/img/card/electron.png" /></div>
            <div class="cart-text-txt">


                <h3 class="h3">Виртуальная карта</h3>
                <p class="p">Подарите карту в электронном виде — в индивидуальном дизайне и с приятным сообщением. </p>
            </div>
        </div>



        </div>
    </div>
</section>

<section class="section section--cardOrder">
    <div class="wrapper">
    <?
        // включаемая область для раздела
        $APPLICATION->IncludeFile('/local/include/gift_order.php', Array(), Array(
            "MODE"      => "html",                                           // будет редактировать в веб-редакторе
            "NAME"      => "Бронирование стрельбы",      // текст всплывающей подсказки на иконке
            "TEMPLATE"  => "section_include_template.php"                    // имя шаблона для нового файла
            ));
        ?>
        
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
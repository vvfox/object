(function mediaChecker(global, document, $) {

	function MediaChecker(options) {

		this.options = $.extend(true, {}, this.defaults, options);
		$.proxy(init, this)();

	}

	MediaChecker.prototype.defaults = {
		breakPoints: {
			mobileSmall: 320,
			mobile: 767,
			tabletPortrait: 800,
			tablet: 1024
		}
	};

	function init() {

		var self = this;

		$.each(self.options.breakPoints, function (name, value) {
			self.addBreakpoint(name, value);
		})

	}

	MediaChecker.prototype.check = function (brPoint) {
		return global.matchMedia(getQuery(brPoint)).matches;
	};

	MediaChecker.prototype.addBreakpoint = function (name, brPoint) {

		var self = this,
				fnName = 'is' + global.ucFirst(name);
		self[fnName] = function () { return self.check(brPoint); }

	};

	function getQuery(brPoint) {
		return 'screen and (max-width: '+ brPoint +'px)';
	}

	global.mediaChecker = new MediaChecker;

}(window, document, jQuery));
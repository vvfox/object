(function anchor(global, document, $) {

	function Anchor(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	Anchor.prototype.defaults = {

		speed: 600,
		topOffset: 0

	};

	function init() {

		var self = this;

		self.href = self.$element.data('href') || self.$element.attr('href');

		self.$element.on('click', function (e) {
			e.preventDefault();
			self.$correspondingBlock = self.href ? $(self.href) : $('html');
			if(self.$correspondingBlock.length) {
				self.scrollToBlock(self.options.speed);
			}
		});

	}

	Anchor.prototype.setTopOffset = function (topOffset) {
		this.topOffset = topOffset;
	};

	Anchor.prototype.scrollToBlock = function (speed) {

		if(window.globalScroll) {
			const scrollBar = window.globalScroll,
					targetPosition = window.getOffsetRect(document.querySelector(this.href)).top,
					currentOffset = scrollBar.offset.y,
					position = targetPosition + currentOffset - this.options.topOffset;

			window.globalScroll.scrollTo(0, position, this.options.speed);

		} else {
			const $scrollBlock = $('body, html'),
					topOffset = this.topOffset || this.options.topOffset,
					scrollPosition = this.$correspondingBlock.offset().top - topOffset;

			$scrollBlock.animate({scrollTop: (scrollPosition)}, speed || this.options.speed);
		}

	};

	$.fn.anchor = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('anchor');

			if (!data) {
				data = new Anchor(this, typeof options === 'object' && options);
				$this.data('anchor', data);
			}

		});

	};

	$.fn.anchor.Constructor = Anchor;

}(window, document, jQuery));
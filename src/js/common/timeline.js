(function timeline(global, document, $) {

	function Timeline(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	Timeline.prototype.defaults = {

		structure: {
			progressBar: '.timeline__progress',
			pointsBlock: '.timeline__points'
		},
		playEvent: 'play',
		stopEvent: 'stop',
		pointClass: 'point',
		loop: false

	};

	function init() {

		var self = this,
				$video = self.options.$video,
				playEvent = self.options.playEvent,
				stopEvent = self.options.stopEvent;

		self.$window = $(window);

		$.proxy(setStructure, self)();

		if(self.options.timelinePoints){
			$.proxy(initPoints, self)();
		}

		$video
				.on(playEvent, function () {
					self.state = playEvent;
					self.requestId = window.requestAnimationFrame($.proxy(updateTimeline, self));
				})
				.on(stopEvent, function () {
					self.state = stopEvent;
				});

		self.$element.on('click', function (event) {
			var progress = $.proxy(getProgress, self, event)(),
					curTime = self.options.getDuration() * progress;
			self.options.setVideoTime(curTime);
			$.proxy(setVideoProgress, self, progress)();
		});
	}

	function initPoints() {

		var self = this,
				duration = self.options.getDuration(),
				pointClass = self.options.pointClass;

		$.each(self.options.timelinePoints, function (index, point) {

			var $point = $('<div class="' + pointClass + '">' + point.name + '</div>'),
					leftPos = 100 * (point.time / duration) + '%';

			$point.data('index', index);
			$point.css({left: leftPos});

			self.$pointsBlock.append($point)

		});

		self.$points = self.$pointsBlock.find('.' + pointClass);

		self.$points.on('click', function () {

			var $self = $(this),
					index = $self.data('index'),
					curTime = self.options.timelinePoints[index].time,
					duration = self.options.getDuration(),
					progress = curTime / duration;

			self.options.setVideoTime(curTime);
			$.proxy(setVideoProgress, self, progress)();

			return false;

		});

	}

	function getProgress(event) {

		var self = this,
				$timeline = self.$element,
				w,
				x;

		if(event){
			w = $timeline.width();
			x = event.pageX - $timeline.offset().left;
		}
		else {
			w = self.options.getDuration();
			x = self.options.getCurrentTime();
		}

		return x / w;
	}

	function setVideoProgress(progress) {

		var self = this;

		if(!self.options.loop && progress === 1){

			$.proxy(stopProgress, self)();
			if(self.options.onStop)
				self.options.onStop();

		}

		self.$progressBar.css({
			width: 100 * progress + '%'
		});

	}

	function updateTimeline() {
		var self = this;
		if(self.state === self.options.stopEvent) {
			$.proxy(stopProgress, self)();
		}
		else {
			const progress = $.proxy(getProgress, self)();
			$.proxy(setVideoProgress, self, progress)();
		}
		window.requestAnimationFrame($.proxy(updateTimeline, self))
	}

	function stopProgress() {
		var self = this;
		if(self.requestId) {
			cancelAnimationFrame(self.requestId);
			$.proxy(setVideoProgress, self, 0)();
		}
	}

	function setStructure() {

		var self = this,
				structure = self.options.structure;

		$.each(structure, function (name, value) {

			self['$' + name] = self.$element.find(value);

		});

	}

	Timeline.prototype.destroy = function () {

		var self = this,
				$video = self.options.$video,
				playEvent = self.options.playEvent;

		$.proxy(stopProgress, self)();

		self.$element.data('timeline', null);

		$video.off(playEvent, updateTimeline);
	};

	$.fn.timeline = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('timeline');

			if (!data) {
				data = new Timeline(this, typeof options === 'object' && options);
				$this.data('timeline', data);
			}

		});

	};

	$.fn.timeline.Constructor = Timeline;

}(window, document, jQuery));
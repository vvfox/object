(function formalizer(global, document, $, undefined) {

	function Formalizer(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	Formalizer.prototype.defaults = {

		structure: {
			children: {
				input: 'input',
				select: 'input.select',
				dayRadio: 'input.day',
				file: 'input.file',
				rangeSlider: 'input.range-slider',
				timeSlider: 'input.range-slider.time-slider',
				agreement: 'input.agreement',
				textarea: 'input.textarea',
				options: 'input-group .options',
				submit: 'submit'
			}
		},
		fn: {

			name: function ($input) {

				$input.on('keypress', function (event) {

					var keyChar = global.getChar(event);
					if(keyChar === null || !keyChar.match(/[a-zа-яА-ЯёЁ ]/i))
						return false;

				});
			},
			phone: function ($input, options) {
				$input.inputmask(options);
			},
			range: function ($input, options) {
				engageRangeSlider($input, options);
			},
			date: function ($input, options) {
				console.log(options);

				// console.log('hello, i datepicker');
				$input.calendar(options);

			},
			numeric: function ($input) {
				$input.on('keypress', function (event) {

					var keyChar = global.getChar(event),
							max = $input.data('max'),
							value = $input.val();
					if(keyChar === null || !keyChar.match(/[0-9]/i))
						return false;
					if(value >= max) {
						$input.val(max);
						return false;
					}
				});

			}

		},
		pluginOpts: {
			range: {

			},
			phone: {
				mask: '+7(999)-999-99-99'
			},
			date: {
				format: 'd.m.Y'
			}
		},
		validator: {
			options: {
				modules : 'security',
				showHelpOnFocus : false,
				addSuggestions : false,
				borderColorOnError: '',
				onValidate: function(result) {

					console.log(result);

				}
			},
			rules: {
				common: {
					'validation-error-msg': ' '
				},
				name: {
					'validation': 'required length',
					'validation-length': 'min2',
					'validation-error-msg': 'Укажите имя'
				},
				password: {
					'validation': 'required strength',
					'data-validation-strength': 2
				},
				email: {
					'validation': 'required email',
					'validation-error-msg': 'Укажите корректный e-mail'
				},
				phone: {
					'validation': 'required custom',
					'validation-regexp': '^\\+\\d{1}\\(\\d{3}\\)-\\d{3}-\\d{2}-\\d{2}$',
					'validation-error-msg': 'Укажите корректный телефон'
				},
				date: {
					'validation': 'required',
					
				},
				required: {
					'validation': 'required'
				}
			}

		},
		url: '/ajax/request_form.php',
		successCallback: function () {
			console.log('ok');
		},
		errorCallback: function () {
			console.log('error');
		},
		removeLabels: false,
		async: true,
		watchDay: false

	};

	function init() {

		var self = this,
				active = global.classes.active;

		self.$element.attr('id', self.$element.attr('name'));

		$.each(self.options.structure.children, function (name, value) {

			self['$' + name] = self.$element.find('.' + value);

		});

		self.$input.not(':disabled').each(function (index, input) {

			var $input = $(input),
					type = $input.data('type') || $input.data('input-type');

			if(type && self.options.validator.rules[type])
			{
				var validatorData = $.extend({}, self.options.validator.rules.common, self.options.validator.rules[type]);
				$.each(validatorData, function (name, value) { $input.attr('data-' + name, value); });
			}
			if(type && self.options.fn[type])
			{
				self.options.fn[type]($input, self.options.pluginOpts[type] || undefined);
			}

		});

		self.$dayRadio.each(function (ind, el) {

			var $el = $(el),
					day = $el.attr('id').split('-').pop();

			self['$' + day + 'Radio'] = $el;

		});

		if(self.$dayRadio.length && self.$timeSlider.length && self.options.watchDay)
			$.proxy(watchDay, self)();

		if(self.options.removeLabels)
			$.proxy(removeLabels, self)();

		self.$select.on('change', function () {
			var $self = $(this),
					value = $self.val(),
					$group = $self.closest('.input-group'),
					$label = $group.find('.label'),
					hidden = global.classes.hidden;

			$self.validate();

			if(value && self.options.removeLabels)
				$label.addClass(hidden);

		});

		self.$select.on('click', function () {
			var $self = $(this),
					$group = $self.closest('.input-group');

			$group.toggleClass(active);
		});

		self.$options.on('click', '.item', function () {

			var $self = $(this),
					$group = $self.closest('.input-group'),
					$input = $group.find('.input'),
					value = $self.data('item');

			$input.val(value);
			$input.change();
			$group.removeClass(active);

		});

		global.autosize(self.$textarea);

		$.validate($.extend(
				{},
				{form: '#' + self.$element.attr('id')},
				self.options.validator.options
				));

		// self.$agreement.on('click', function () {

		// 	if(self.$agreement.prop('checked')) {
		// 		self.$agreement.removeClass('error');
		// 	}

		// });

		self.$submit.on('click', function () {


			if(self.$element.isValid({}, {
						onElementValidate : function(valid, $el, $form, errorMess) {

							console.log(valid)

							$el.validate();

						}
					}, false)) {

				// if(self.$agreement.length && !self.$agreement.prop('checked')) {
				// 	self.$agreement.addClass('error');
				// 	return false;
				// }

				$.proxy(sendForm, self)();
			}

			return false;

		});

	}

	function removeLabels() {

		var self = this,
				hidden = global.classes.hidden;

		self.$input.on('focus', function () {
			var $self = $(this),
					$group = $self.closest('.input-group'),
					$placeholder = $group.find('.placeholder');
			$placeholder.addClass(hidden);
		}).on('blur', function () {
			var $self = $(this),
					value = $self.val(),
					$group = $self.closest('.input-group'),
					$placeholder = $group.find('.placeholder');

			if(!value)
				$placeholder.removeClass(hidden);

		});

	}

	function engageRangeSlider(element, options) {

		var min = element.data('min'),
				max = element.data('max'),
				from = min,
				to = max,
				step = element.data('step'),
				arValue = element.val() ? element.val().split('-') : [],
				defaults = {
					type: 'double',
					min: min,
					max: max,
					from: from,
					to: to,
					step: step,
					hide_min_max: true,
					input_values_separator: '-',
					onFinish: function () {
						resetSliderInput();
					},
					onChange: function () {
						resetSliderInput();
					},
					onStart: function () {
						setTimeout(function () {
							resetSliderInput();
						}, 1000);
					},
					onUpdate: function () {
						resetSliderInput();
					}
				};

		if(arValue.length > 1){
			defaults.from = arValue[0];
			defaults.to = arValue[1];
		}

		function resetSliderInput() {
			if(element.val() === (min + '-' + max))
				element.val('');
		}

		element.ionRangeSlider($.extend({}, defaults, options));
	}

	function sendForm() {

		var self = this;

		if(self.options.async)
			$.proxy(sendFormAsync, self)();
		else self.element.submit();


	}

	function sendFormAsync() {

		var self = this,
				url = self.options.url,
				successCallback = self.options.successCallback,
				errorCallback = self.options.errorCallback,
				data = new FormData(self.element);

		$.ajax({
			url: url,
			data: data,
			type: "POST",
			cache: false,
			contentType: false,
			processData: false,
			success: function(response){

				if(successCallback)
					successCallback(response);

			},
			error: function () {

				if(errorCallback){
					errorCallback();
				}

			}
		});

	}

	function watchDay() {

		var self = this,
				$todayRadio = self.$todayRadio,
				$tomorrowRadio = self.$tomorrowRadio,
				rangeSlider = self.$timeSlider.data('ionRangeSlider'),
				$todayLabel = $todayRadio.parent().find('.label[for="' + $todayRadio.attr('id') + '"]'),
				from = self.$timeSlider.data('min'),
				to = self.$timeSlider.data('max'),
				date,
				hour;

		function isWorkingHour(hour, to) {

			return hour < to

		}

		function checkDay() {

			if(!isWorkingHour(date.getHours(), to)){
				$tomorrowRadio.prop('checked', true);
				$todayLabel.addClass(window.classes.hidden);
			}
			else {
				$todayRadio.prop('checked', true);
				$todayLabel.removeClass(window.classes.hidden);

				rangeSlider.update({
					from_min: hour,
					from: hour
				});
			}
		}

		function updateTime() {
			date = new Date;
			hour = isWorkingHour(date.getHours(), to) ? date.getHours() + 1 : from;
			checkDay();
		}

		updateTime();

		setInterval(function () {

			updateTime();

		}, 1000*30);

		$todayRadio.on('click', function () {

			rangeSlider.update({
				from_min: hour,
				from: hour
			});

		});

		$tomorrowRadio.on('click', function () {

			rangeSlider.update({
				from_min: from,
				from: from
			});

		});

	}

	Formalizer.prototype.setUrl = function(url) {

		this.options.url = url;

	};

	$.fn.formalizer = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('formalizer');

			if (!data) {
				data = new Formalizer(this, typeof options === 'object' && options);
				$this.data('formalizer', data);
			}

		});

	};

	$.fn.formalizer.Constructor = Formalizer;

}(window, document, jQuery));
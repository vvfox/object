$(function () {

	(function freezer(window, document, $) {

		function Freezer() {

			this.$body = $('html, body');
			this.$content = $('.content');
			this.$window = $(window);
			this.st = this.$window.scrollTop();
			this.freezeCount = 0;

		}


		Freezer.prototype.freeze = function () {

			var self = this,
					st = self.$window.scrollTop();

			if(self.freezeCount === 0) {
				self.$body.addClass(window.classes.freeze);
				self.$content.css({
					'margin-top': -st
				});
				self.st = st;
			}

			self.freezeCount++;

		};

		Freezer.prototype.unFreeze = function (reset) {

			var self = this,
					st = self.st;

			self.freezeCount = Math.max(0, self.freezeCount - 1);

			if(self.freezeCount === 0) {
				self.$body.removeClass(window.classes.freeze);
				self.$content.removeAttr('style');
				if(!reset && st > 0) {
					self.$window.scrollTop(st);
				}

				self.st = 0;
			}

		};

		window.Freezer = new Freezer();


	}(window, document, jQuery));

});

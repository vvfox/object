(function parallax(global, document, $) {

	function Parallax(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		this.$parent = this.$element.parent();
		$.proxy(init, this)();

	}

	Parallax.prototype.defaults = {

		speed: 0.05,
		units: '%'

	};

	function init() {

		var self = this;

		$(window).on('scroll', function () {

			var scrollTop = $(this).scrollTop(),
					elParentOffset = self.$parent.offset().top,
					translate = Math.max(0, scrollTop - elParentOffset) * self.options.speed;

			setParallax(self.$element, translate, self.options.units);

		});

	}

	function setParallax($element, translate, units) {

		$element.css({
			'transform': 'translateY(' + translate + units + ')'
		});

	}

	$.fn.parallax = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('parallax');

			if (!data) {
				data = new Parallax(this, typeof options === 'object' && options);
				$this.data('parallax', data);
			}

		});

	};

	$.fn.parallax.Constructor = Parallax;

}(window, document, jQuery));
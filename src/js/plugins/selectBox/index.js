
import SelectBox from './class';

const pluginName = 'selectBox';

$.fn[pluginName] = function (options, ...params) {
	SelectBox.registerPlugin(this, pluginName, SelectBox, options, ...params)
};

$.fn[pluginName].Constructor = SelectBox;
export default  {
	structure: {
		header: '.discloser__header',
		content: '.discloser__content'
	},
	activeClass: window.classes.active,
	slide: true,
	isInitiallyOpen: false
}
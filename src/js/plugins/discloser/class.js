
import PluginBase from '../pluginBase';
import defaults from './defaults';

export default class Discloser extends PluginBase {

	constructor(element, options) {
		super(element, options, defaults);
		this.init();
		this.isOpen = this.options.isInitiallyOpen;
	}

	init() {
		this.setStructure();
		this.$header.on('click', () => {
			this.toggleOpen();
		});
		if(this.options.isInitiallyOpen) {
			this.open();
		}
	}

	toggleOpen() {
		this.$element.toggleClass(this.options.activeClass);
		this.isOpen = !this.isOpen;
		if (this.options.slide) {
			if(this.isOpen) {
				this.$content.slideDown();
			}
			else {
				this.$content.slideUp();
			}
		}
	}

	open() {
		this.$element.addClass(this.options.activeClass);
		this.isOpen = true;
		if (this.options.slide) {
			this.$content.slideDown();
		}
	}

	close() {
		this.$element.removeClass(this.options.activeClass);
		this.isOpen = false;
		if (this.options.slide) {
			this.$content.slideUp();
		}
	}

}

export default class PluginBase {

	constructor(element, options, defaults) {
		this.options = $.extend(true, {}, defaults, options);
		this.element = element;
		this.$element = $(element);
	}

	setStructure() {
		$.each(this.options.structure, (name, value) => {
			this['$' + name] = this.$element.find(value);
		});
	}

	static registerPlugin($context, pluginName, pluginClass, options, ...params) {
		$context.each(function () {
			let $this = $(this),
					data = $this.data(pluginName);
			if ((!options || typeof options === 'object') && !data) {
				data = new pluginClass(this, options);
				$this.data(pluginName, data);
			} else if(typeof options === 'string' && data && typeof data[options] === 'function') {
				data[options].apply(data, params);
			} else {
				throw `${pluginName}: invalid call`;
			}
		});
	}

}
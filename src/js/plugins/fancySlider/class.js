
import PluginBase from '../pluginBase';
import defaults from './defaults';

export default class FancySlider extends PluginBase {

	constructor(element, options) {
		super(element, options, defaults);
		this.init();
	}

	init() {
		const self = this,
				$window = $(window),
				nextKeyCodes = [13, 32, 34, 39, 40],
				prevKeyCodes = [33, 37, 38];
		this.setStructure();
		this.initItems();
		this.curItemNum = 0;
		this.itemCount = this.$items.length;
		this.isOnHold = false;
		this.$pagerCur.find('value').html('0' + (this.curItemNum + 1));
		this.$pagerNext.find('value').html('0' + (this.curItemNum + 2));

		$window
				.on('keydown', function (event) {
					if(!self.isOnHold){
						const keyCode = event.which;
						let direction = self.options.directions.next;
						if (nextKeyCodes.indexOf(keyCode) !== -1) { self.goNext(); }
						if (prevKeyCodes.indexOf(keyCode) !== -1) {
							self.goPrev();
							direction = self.options.directions.prev;
						}
						if(self.options.loop){ self.setLoop(direction); }
					}
				})
				.on('resize', () => {
					this.$img.css({
						height: this.$imgContainer.height(),
						width: this.$imgContainer.width()
					})

				}).resize();
		
		if(!window.mediaChecker.isTablet()){
			this.$element
					.on('mouseenter', () => {
						this.clearLoop();
					})
					.on('mouseleave', () => {
						if(self.options.loop){
							this.setLoop();
						}
					});

			if(this.options.loop){
				this.setLoop();
			}
		}

		if(window.mediaChecker.isTablet()) {
			this.engageSwipe();
		}

	}

	initItems() {
		this.$items.each((index, item) => {
			const $item = $(item);
			if(index === 0){
				$item.addClass(this.options.activeClass + ' ' + this.options.currentClass);
			}
			$item.data('index', index);
		});

	}

	goPrev() {
		this.go(this.options.directions.prev);
	}
	
	goNext() {
		this.go(this.options.directions.next);
	}

	go(direction, index) {
		let curItemNum = this.curItemNum,
				nextItemNum = index,
				actualItemNum = nextItemNum !== undefined ? nextItemNum % this.itemCount : undefined;

		this.isOnHold = true;

		if(direction) {
			if(direction === this.options.directions.prev) {
				nextItemNum = curItemNum - 1;
				actualItemNum = nextItemNum >= 0 ? nextItemNum : this.itemCount + nextItemNum;
				this.$element.addClass(this.options.directions.prev);
				this.$element.removeClass(this.options.directions.next);
			}

			if(direction === this.options.directions.next) {
				nextItemNum = curItemNum + 1;
				actualItemNum = nextItemNum % this.itemCount;
				this.$element.removeClass(this.options.directions.prev);
				this.$element.addClass(this.options.directions.next);
			}
		}

		this.curItemNum = actualItemNum;

		this.setClass(this.options.activeClass, actualItemNum, ($item, className) => {
			$item.addClass(className);
		}, ($item, className) => {
			setTimeout(function () {
				$item.removeClass(className);
			}, 1000);
		});

		this.setCurrent();

		this.animatePager(actualItemNum);

		setTimeout(() => {
			this.isOnHold = false;
		}, 1000);

		this.$element.trigger('fancySlider.change', [curItemNum, actualItemNum])

	}

	setCurrent() {
		const className = this.options.currentClass;
		this.setClass(className, this.curItemNum, ($item, className) => {
			$item.addClass(className);
		}, ($item, className) => {
			$item.removeClass(className);
		});
	}

	setClass(className, index, funcTrue, funcFalse) {
		funcTrue(this.$items.filter((i, v) => $(v).data('index') === index), className);
		funcFalse(this.$items.filter((i, v) => $(v).data('index') !== index), className);
	}

	switchPager() {
		this.$pagerCur.toggleClass(this.options.activeClass);
		this.$pagerNext.toggleClass(this.options.activeClass);
	}

	animatePager(nextItemNum) {
		const nextItemNumStr = Math.floor(nextItemNum / 10) === 0 ? '0' + (nextItemNum + 1) : nextItemNum + 1;
		this.$pager.removeClass(this.options.pagerHoldClass);
		this.$pagerNext.find('.value').html(nextItemNumStr);
		this.switchPager();
		setTimeout(() => {
			this.$pager.addClass(this.options.pagerHoldClass);
			this.$pagerCur.find('.value').html(nextItemNumStr);
			this.switchPager();
		}, 1000);
	}

	setLoop(direction) {
		this.clearLoop();
		this.options.loop = true;
		this.intervalId = setInterval(() => {
			this.go(direction || this.options.directions.next);
		}, this.options.interval);
	}

	clearLoop() {
		if(this.intervalId)
			clearInterval(this.intervalId);
	}

	engageSwipe() {
		const swiper = new Hammer(this.element);
		swiper
				.on('swipeleft', () => { this.onSwipe(this.options.directions.next); })
				.on('swiperight', () => { this.onSwipe(this.options.directions.prev); });
	}

	onSwipe(direction) {
		if(!this.isOnHold){
			this.go(direction);
			if(this.options.loop && !window.mediaChecker.isTablet()){
				this.setLoop(direction);
			}
		}
	}
	

}
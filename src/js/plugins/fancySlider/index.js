
import FancySlider from './class';

const pluginName = 'fancySlider';

$.fn[pluginName] = function (options, ...params) {
	FancySlider.registerPlugin(this, pluginName, FancySlider, options, ...params)
};

$.fn[pluginName].Constructor = FancySlider;
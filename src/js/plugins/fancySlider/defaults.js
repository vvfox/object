export default {
	structure: {
		items: '.item',
		imgContainer: '.b-img',
		img: '.img',
		pager: '.b-pager',
		pagerCur: '.b-pager .num--cur',
		pagerNext: '.b-pager .num--next'
	},
	directions: {
		prev: 'prev',
		next: 'next'
	},
	interval: 5000,
	loop: true,
	activeClass: window.classes.active,
	pagerHoldClass: window.classes.hold,
	currentClass: 'current'
};
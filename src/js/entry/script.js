import './../common';
import './../page_scripts/gift';
import './../page_scripts/education';
import './../page_scripts/events';

let ytID = function  (url){
    var video_id, result;
    if(result = url.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/)){
        video_id = result.pop();
    } else if(result = url.match(/youtu.be\/(.{11})/)){
        video_id = result.pop();
    }
    return video_id;

}

window.ytID = ytID;
function get_youtube_info (el, quality){
    let $this = $(el),
    href=$this.attr('href'),url = href;

    console.log(el);
    if(url){
        var video_id, thumbnail, result;
        video_id = ytID(url);

        if(video_id){
            if(typeof quality == "undefined"){
                quality = 'high';
            }
        
            var quality_key = 'maxresdefault'; // Max quality
            if(quality == 'low'){
                quality_key = 'sddefault';
            }else if(quality == 'medium'){
                quality_key = 'mqdefault';
            } else if (quality == 'high') {
                quality_key = 'hqdefault';
            }

            var thumbnail = "http://img.youtube.com/vi/"+video_id+"/"+quality_key+".jpg";
            // var url = 'https://www.youtube.com/watch?v=' + video_id;
  
            
                    let $img = $('<img>'),
                        $div_img = $('<div>'),
                        $div_title = $('<div>');

                    $div_img.addClass('video--thumbnail');
                    $div_title.addClass('video--link__title');
                    $.getJSON('https://noembed.com/embed', {format: 'json', url: url}, function(data) {
                        console.log(data);
                        $this.data('title', data.title);
                        $div_title.html(data.title);   
                    });

                    
                    $img.attr('src', thumbnail);
                    // console.log(el.href);
                    $div_img.append($img);
                    $this.prepend($div_title);
                    $this.prepend($div_img);
                    
            return thumbnail;
        }
    }
    return false;
}

$(function () {


    $('a[href^="#"]').click(function () {
        var idel= $(this).attr('href'), 
            $el = $(idel);

        if(idel != '#' && !($(this).hasClass('tab'))){
            $('html,body').animate({scrollTop: $el.offset().top},'slow');
            return false;
        }
       
    });

    let tooltp_class = 'tooltype__text';

    $('.tooltype').hover(function(){
        let $this = $(this),
            tooltp_txt = $this.data('tooltype');
            if(!$('.' + tooltp_class).length){
                $this.append(['<div class="' + tooltp_class + '">',tooltp_txt,'</div>'].join(''));
            }
    }, function(){
        $('.' + tooltp_class).remove();


    })


    let $tabs = $('.tabs'),
        $tab = $('.tab');

    $tab.click(function () {

        let $this = $(this), 
            idtab = $this.attr('href');

        $(this).closest('.tabs').find('.tab').removeClass('select');
        $(this).addClass('select');

        if($(idtab).length){
           let $tb_container =  $(idtab).closest('.tab-container');
           $tb_container.find('.tab-content').removeClass('show');
           $(idtab).addClass('show');
        }
        return false;
    });
    
    $tabs.each(function(ind, el){
        let $tab_first = $(el).find('.tab').first();
        $tab_first.click();

    });

    let $price = $('input[name="price"]');
    $('.courses--item').click(function(){

        let $this = $(this),
            name = $this.data('name'),
            price = $this.data('price'),
            product_id = $this.data('id');
        $('.shooting--popup').addClass('show');

        $('.shooting--popup__title').html(name);
        $('input[name="price"]').val(price);//product_id
        $('input[name="product_id"]').val(product_id);

        $('input[name="count_shooters"]:checked').change();
        // product_id
        // price
        return false;

    });


    // $price 

    $('.shooting--popup__shadows').click(function(){
        $('.shooting--popup').removeClass('show');

    });

    $('input[name="count_shooters"]').change(function(){

        let count_shooters = parseInt($(this).val()),
            price = parseFloat($price.val()),
            sum = (count_shooters*price).toFixed(2);

        $('.sum-value').html(sum);

    });

    let $shooting_popup = $('.shooting--popup');

    if($shooting_popup.length){
        $('.shooting--booking__step').first().addClass('active');
       // $('.shooting--booking__calendar-input').calendar({});
    }
    (function common(window, $) {
        let $form  = $('#shooting--booking__form'),
            $formParent = $form.closest('.b-form')

            var minDate = new Date;
            minDate.setDate(minDate.getDate() +1);

        function onResponse($form, $formParent, class_name, resp){

            console.log($form); 
            console.log($formParent);
            console.log(class_name);
            if(typeof resp !=='undefined'){



            }
            $form.addClass('form--' + class_name);

        }
        $form.formalizer({
            url: '/ajax/booking.php',
            // watchDay: true,
            successCallback: function (resp) {

                onResponse($form, $formParent, window.classes.success, resp);
            },
            errorCallback: function () {
                onResponse($form, $formParent, window.classes.error);
            },
            pluginOpts: {
                range: {
                    postfix: '.00',
                    min_interval: 1
                },
                phone: {
                    mask: '+9(999)-999-99-99'
                },
                date:{
                    pickmeup:{
                        flat : true,
                        hide_on_select: false,
                        select_year: false,
                        min: minDate
                    }
                }
                
            }
        });

        $('.shooting--booking__calendar-input').change(function(){
            console.log(this.value);
            change_time('day', this.value);


        });
        $('.time-today input').change(function(){
            console.log(this.value);
            change_time('hour', this.value);

        });

        let change_time = function(prop, value) {
            let day_txt =$('.shooting--booking__calendar-input').val(),

                time_txt = $('.time-today input:checked').val(),
                ar_day = day_txt.split('.'),
                day_date = new Date(ar_day[2], ar_day[1], ar_day[0]),
                months = ['',"января", "яевраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
                s_day = [ar_day[0], months[parseInt(ar_day[1])]].join(' '),
                
                tooltype_text = [
                    s_day,
                    time_txt
                ].join(' ');
                if(day_txt!='' && time_txt!='' && prop=='hour'){
                    $('.tab-contact').click();
                }

            // console.log(tooltype_text);

            $('.tab-time .tooltip').html(tooltype_text);

             
            


        }

        

    }(window, jQuery));




    let $courses_list = $('.courses--list');


    

    if($courses_list.length){

        let init_flickity = false,
        fl_options={
            // options
            cellAlign: 'center',
            contain: true,
            // autoPlay: 10000,
            // freeScroll: true,
            wrapAround: true,
            prevNextButtons: false,
        };

        $(window).resize(function(){
            if($(window).width()<767){

                if(!init_flickity){
                    $courses_list.flickity(fl_options);
                    init_flickity = true;
    
                }
               
    
            }else{
                if(init_flickity){
                    $courses_list.flickity('destroy');
                    init_flickity = false;
                }
    
            }

        }).resize();
        
        
    

    }

   
    $('.popup-youtube, .popup-vimeo, .popup-gmaps, .popup-yamaps').magnificPopup({
        disableOn: 0,
        type: 'iframe',
        preloader: true,
        fixedContentPos: true
    });



    $('.video--link a').each(function (ind, el) {
        get_youtube_info(el);
    });



});
(function gift_scroll(global, document, $) {

   let $pageGift = $('.page-gift');
   if($pageGift.length){

        $(window).scroll(function (e) {
            // Get the position of the location where the scroller starts.
            var scroller_anchor = $(".scroller_anchor").offset().top;
        
            var lastId,
            topMenu = $(".tabs--nav"),
                topMenuHeight = topMenu.outerHeight() + 15,
                // All list items
                menuItems = topMenu.find("a"),
                // Anchors corresponding to menu items
                scrollItems = menuItems.map(function () {
                    var item = $($(this).attr("href"));
                    if (item.length) {
                        return item;
                    }
                });
        
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;
        
            // Get id of current scroll item
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < fromTop) return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";
        
            if (lastId !== id) {
                lastId = id;
                // Set/remove active class

                console.log(id);
                menuItems.removeClass("active").parent().find('.tab-' + id ).addClass("active");
        
                // $('option[value="#' + id + '"]').attr('selected','selected');
            }
        });
        $('.tabs--nav a.tab[href^="#"]').click(function () {
            var idel= $(this).attr('href'), 
                $el = $(idel);

            if(idel != '#'){
                $('html,body').animate({
                    scrollTop: $el.offset().top - $('.tabs--nav').height()
                },'slow');

                return false;
            }
        
        });
        $('.card-images').each(function(ind, el){

            $carusel_main =$(el).find('.carousel-main');
            $carusel_main.flickity({
                prevNextButtons: false,
                pageDots: false
            });
            let id=$carusel_main.attr('id');
            // 2nd carousel, navigation
            $(el).find('.carousel-nav').flickity({
                asNavFor: '#' + id, //'.carousel-main',
                contain: true,
                pageDots: false,
                prevNextButtons: false
            });

        });
        $('#card-custom-sum').change(function(){
            console.log(this.value);
            let $ipg = $(this).closest('.input-group');
            if(this.value !=''){
                $ipg.addClass('filled');
            }else{
                $ipg.removeClass('filled');
            }
        }).change();
        

        $('.card--buy').click(function(){

            let $card = $(this).closest('.card-item'),
                id = $card.data('id'),
                name = $card.data('name'),
                cost = $card.data('cost');

            if(id==6){
                cost = $('#card-custom-sum').val();
            }


            

            return false;
        });
    }   

    let $form = $('#card--order__form'),
    $formParent = $form.closest('.b-form')

            var minDate = new Date;
            minDate.setDate(minDate.getDate() +1);

        function onResponse($form, $formParent, class_name, resp){

            console.log($form); 
            console.log($formParent);
            console.log(class_name);
            if(typeof resp !=='undefined'){



            }
            $form.addClass('form--' + class_name);

        }
        $form.formalizer({
            url: '/ajax/events.php',
            // watchDay: true,
            successCallback: function (resp) {

                onResponse($form, $formParent, window.classes.success, resp);
            },
            errorCallback: function () {
                onResponse($form, $formParent, window.classes.error);
            },
            pluginOpts: {
                range: {
                    postfix: '.00',
                    min_interval: 1
                },
                phone: {
                    mask: '+9(999)-999-99-99'
                },
                date:{
                    pickmeup:{
                        // flat : true,
                        // hide_on_select: false,
                        select_year: false,
                        min: minDate
                    }
                }
                
            }
        });

        $('.card-item .card--buy').click(function(){
            let $this =$(this), 
                $item = $this.closest('.card-item'),
                cost = $item.data('cost');

                if($item.find('#card-custom-sum').length){

                    cost =  $item.find('#card-custom-sum').val();

                }

            

            $('.basket').append([


                '<div class="basket--item" data-cost="',cost,'">',
                    '<div>',$item.data('name'), '</div>',
                    '<div>Номинал ',cost,'₽', '</div>',
                    '<div>',
                    '<div class="input--number__overlay">',
                    '<span class="minuse"> — </span>',
                    '<input class="input input--number" name="basket[',$item.data('id'), ']" value="1">',
                    '<span class="pluse"> + </span>',
                    '</div>',
                    '</div>',
                    '<div class="basket--item__summary summary">',cost ,'₽', '</div>',
                '</div>',

            ].join(''));
            change_count();

        });

        $('body').on('click', '.input--number__overlay span', function(){
            console.log(this);

            let $this = $(this),
                $overlay = $this.closest('.input--number__overlay'),
                $input = $overlay.find('input'),
                count = parseInt($input.val());
            if($this.hasClass('pluse')){
                count++;
                $input.val(count);
                $input.change();

            }else{
                if(count!=1){
                    count--;
                    $input.val(count);
                    $input.change();
                }else{
                    $this.closest('.basket--item').remove();
                }

            }
            

            return false;
        });
        $('body').on('blur change keyup', '.input--number', function(){
            console.log(this.value);

            let count = this.value,
                $item = $(this).closest('.basket--item'),
                cost = $item.data('cost'),
                $summary = $item.find('.summary');
            
                $summary.html((cost * count) + '₽' );

                change_count();
            


        });

        let change_count = function(){

            let $items  = $form.find('.basket--item'),
                sum_basket  = 0,
                $btn_submit = $form.find('.submit');
                
            $items.each(function(ind, item){

                let $item = $(item),
                    count = $item.find('.input').val(),
                    cost = $item.data('cost'),
                    sum_item = count*cost;
                sum_basket += sum_item;
            });
            $btn_submit.find('.cost').html(sum_basket + '₽');
                

        };
        change_count();

}(window, document, jQuery));
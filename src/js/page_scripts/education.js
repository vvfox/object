
(function educationPage(global, document, $) {

    

    $('.edu--toc__list ol li a').click(function () {
        var idel= $(this).attr('href'), 
        $el = $(idel);


        $('html,body').animate({
            scrollTop: $el.offset().top 
        },'slow');
        return false;
    });

    

    

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function getYouTubeInfo(vidid) {
        $.ajax({
        url: "http://gdata.youtube.com/feeds/api/videos/"+vidid+"?v=2&alt=json&orderby=published&prettyprint=true",
        dataType: "jsonp",
        success: function (data) {parseresults(data)}
        });
    }
    function parseresults(data){
        console.log(data);

    }
    function onPlayerReady(event) {

        console.log(event);
        event.target.playVideo();
      }

      var done = false;
      function onPlayerStateChange(event) {
        // if (event.data == YT.PlayerState.PLAYING && !done) {
        //   setTimeout(stopVideo, 6000);
        //   done = true;
        // }
      }
      function stopVideo() {
        // player.stopVideo();
      }
let players = {};

$('.less--item .video--link a').click(function(){
    let $this = $(this),
        href = $this.attr('href'),
        $item = $this.closest('.less--item'),
        $video_item = $item.find('.less--item__video'),
        $video_item__txt = $video_item.find('.less--item__video-txt'),
        $video_item__iframe = $video_item.find('.less--item__video-iframe'),
        $player = $video_item__iframe.find('.player') ;
        let youtube_id = ytID(href);

        $video_item.slideDown();
        $('html,body').animate({
            scrollTop: $video_item.offset().top 
        },'slow');

        // getYouTubeInfo(youtube_id);
        $video_item__txt.html('');

        console.log($player.prop("tagName"));

        if($player.prop("tagName")=='IFRAME'){

            player =  players[$player.attr('id')];
            player.loadVideoById({'videoId': youtube_id, 'startSeconds': 0});


        }else{
            player = new YT.Player($player.attr('id'), {
                height: '360',
                width: '640',
                videoId: youtube_id,
                events: {
                  'onReady': onPlayerReady,
                  'onStateChange': onPlayerStateChange
                }
              });

              players[$player.attr('id')] = player;

              
            //   $player.data('pl', player);

        }

       

        // var url = 'https://www.youtube.com/watch?v=' + youtube_id;
  
        // $.getJSON('https://noembed.com/embed', {format: 'json', url: url}, function(data) {
        //         //alert(data.title);
        //     console.log(data);
            
            $video_item__txt.append('<h2 class="h2">' + $this.data('title')  + '</h2>');
        // });
        
    


    return false;



});



}(window, document, jQuery));
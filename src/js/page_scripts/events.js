(function eventsPage(global, document, $) {


let $page= $('.page-activity');
if($page.length){

    let fl_options={
        // options
        cellAlign: 'left',
        contain: false,
        // autoPlay: 10000,
        // freeScroll: true,
        wrapAround: true,
        prevNextButtons: false,
    };
    $('.menu--gallery').flickity(fl_options);

    let $form  = $('#events--booking__form'),
            $formParent = $form.closest('.b-form')

            var minDate = new Date;
            minDate.setDate(minDate.getDate() +1);

        function onResponse($form, $formParent, class_name, resp){

            console.log($form); 
            console.log($formParent);
            console.log(class_name);
            if(typeof resp !=='undefined'){



            }
            $form.addClass('form--' + class_name);

        }
        $form.formalizer({
            url: '/ajax/events.php',
            // watchDay: true,
            successCallback: function (resp) {

                onResponse($form, $formParent, window.classes.success, resp);
            },
            errorCallback: function () {
                onResponse($form, $formParent, window.classes.error);
            },
            pluginOpts: {
                range: {
                    postfix: '.00',
                    min_interval: 1
                },
                phone: {
                    mask: '+9(999)-999-99-99'
                },
                date:{
                    pickmeup:{
                        // flat : true,
                        // hide_on_select: false,
                        select_year: false,
                        min: minDate
                    }
                }
                
            }
        });

        $('input[name="count"], input[name="param[6]"], input[name="param[7]"]').on('change keyup',function(){


            let count = $form.find('input[name="count"]').val(),
                parmas6 = $form.find('input[name="param[6]"]:checked').closest('.radio-item').data('cost'),
                parmas7 = $form.find('input[name="param[7]"]:checked').closest('.radio-item').data('cost'),
                $order_price = $('#order--price'),
                sum_order = parseInt(count) * (parseFloat(parmas6) + parseFloat(parmas7));


                    console.log(count);
                    console.log(parmas6);
                    console.log(parmas7);

                $order_price.html(sum_order + ' ₽' );


        });
        $('input[name="count"]').change();


}
    
}(window, document, jQuery));